<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTienensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tienens', function (Blueprint $table) {
            $table->integer('idA')->unsigned();
            $table->foreign('idA')->references('id')->on('asociaciones');
            $table->integer('idU')->unsigned();
            $table->foreign('idU')->references('id')->on('users');
            $table->integer('idC')->unsigned();
            $table->foreign('idC')->references('id')->on('cargos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tienens');
    }
}
