<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Actividades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('actividades', function (Blueprint $table) {
            $table->increments('id');

$table->integer('idA')->unsigned();
            
$table->string('nombreA');
$table->string('objetivoA');
$table->date('fechaA');
$table->string('horaA');
$table->string('lugarA');
$table->string('descripcionA');
$table->foreign('idA')->references('id')->on('asociaciones');

$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
