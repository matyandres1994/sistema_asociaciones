<table class="table table-inverse">
    <thead>
        <tr>
            <th class="text-center">Nombre</th>
            <th class="text-center">Objetivo</th>           
            <th class="text-center">Fecha</th>
            <th class="text-center">Hora</th>
            <th class="text-center">Lugar</th>
            <th class="text-center">Descripcion</th>

        <th class="text-center"><a id="btnIngresar" href=""><i class="fas fa-plus"></i></a></th>
                                        
        </tr>
    </thead>
    <tbody>
        
        @foreach ($Actividades as $element)
        <tr id={{ $element->id }}>
            <th class="text-center" class="text-center">{{ $element->nombreA }}</th>
            <th class="text-center">{{ $element->objetivoA}}</th>
            <th class="text-center" class="text-center">{{ $element->fechaA }}</th>
            <th class="text-center">{{ $element->horaA}}</th>
            <th class="text-center">{{ $element->lugarA}}</th>
            <th class="text-center">{{ $element->descripcionA}}</th>

          <th class="text-center"><a href="" class="btnEliminar" > <i class="fas fa-trash-alt"></i></a>
            <a href="" class="btnEdit" ><i class="fas fa-align-justify"></i></a></th>
        </tr>
        @endforeach
    </tbody>
</table>
