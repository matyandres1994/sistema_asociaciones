<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadosA extends Model
{
    //
    protected $fillable = [
        'nombre'
    ];
}
