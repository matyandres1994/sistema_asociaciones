<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tienens extends Model
{
    //
    protected $fillable = [
        'idA', 'idU', 'idC'
    ];
}
