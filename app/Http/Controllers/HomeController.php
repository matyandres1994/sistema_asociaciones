<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Auth::user()->rol == 'rector') {
             return view('rector.rector');
        }
        if (\Auth::user()->rol == 'dde') {
             return view('dde.dde');
        }
        if (\Auth::user()->rol == 'administrador') {
             return view('admin.admin');
        }
        if (\Auth::user()->rol == 'alumno') {
             return view('estudiantes.estudiantes');
        }

    
        
    }
    
}
