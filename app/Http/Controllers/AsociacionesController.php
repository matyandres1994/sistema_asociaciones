<?php

namespace App\Http\Controllers;

use App\Asociaciones;
use App\User;
use Illuminate\Http\Request;

class AsociacionesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $id = \Auth::user()->id;

        $asociaciones = \DB::table('asociaciones')->join('estadosA', 'asociaciones.idE', '=', 'estadosA.id')->where('asociaciones.idE', '1')->select('asociaciones.nombre', 'asociaciones.id', 'asociaciones.descripcion', 'asociaciones.objetivo')->get();

        if (\Auth::user()->rol == 'rector') {
            return view('rector.tablaasociaciones')->with('asociaciones', $asociaciones);
        }
        if (\Auth::user()->rol == 'dde') {
            return view('dde.tablaasociaciones', compact('asociaciones'));
        }
        if (\Auth::user()->rol == 'administrador') {
            return view('admin.tablaasociaciones')->with('asociaciones', $asociaciones);
        }
        if (\Auth::user()->rol == 'alumno') {
            return view('estudiantes.tablaasociaciones')->with('asociaciones', $asociaciones);
        }

    }

    public function miembros($id)
    {

        $Miembros = \DB::table('users')->join('tienens', 'users.id', '=', 'tienens.idU')->join('cargos', 'tienens.idC', '=', 'cargos.id')->where('tienens.idA', $id)->select('users.id', 'users.name', 'users.email', 'cargos.nombre')->get();

        if (\Auth::user()->rol == 'rector') {
            return view('rector.integrantesasociacion', compact('Miembros'));
        }
        if (\Auth::user()->rol == 'dde') {
            return view('dde.integrantesasociacion', compact('Miembros'));
        }
        if (\Auth::user()->rol == 'administrador') {
            return view('admin.integrantesasociacion', compact('Miembros'));
        }
        if (\Auth::user()->rol == 'alumno') {
            return view('estudiantes.integrantesasociacion', compact('Miembros'));
        }
    }

    public function index1()
    {

        $email = \DB::table('users')->select('email', 'id')->get();

        if (\Auth::user()->rol == 'rector') {
            return view("rector.gestionAsociacion", compact('email'));
        }
        if (\Auth::user()->rol == 'dde') {
            return view("dde.gestionAsociacion", compact('email'));
        }
        if (\Auth::user()->rol == 'administrador') {
            return view("admin.gestionAsociacion", compact('email'));
        }
        if (\Auth::user()->rol == 'alumno') {
            return view("estudiantes.gestionAsociacion", compact('email'));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $producto = Asociaciones::create($request->all());
        $producto->save();
        $values1 = array('idU' => $request->presi, 'idA' => $producto->id, 'idC' => 1);
        \DB::table('tienens')->insert($values1);
        $values2 = array('idU' => $request->secre, 'idA' => $producto->id, 'idC' => 2);
        \DB::table('tienens')->insert($values2);
        $values3 = array('idU' => $request->teso, 'idA' => $producto->id, 'idC' => 3);
        \DB::table('tienens')->insert($values3);
        return response()->json([
            'mensaje' => $producto->nombre . ' fue ingresado correctamente y puesto en estado PENDIENTE',
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Asociaciones  $asociaciones
     * @return \Illuminate\Http\Response
     */
    public function eliminar(Request $request)
    {

        $aso = Asociaciones::find($request->id);
        $aso->update([
            'idE' => 3,
        ]);

    }

    public function cuentas()
    {

        $Asociaciones = \DB::table('users')->select('id', 'name', 'email', 'rol')->get();
        return view('admin.vercuentas', compact('Asociaciones'));

    }

    public function show($id)
    {

        $Asociaciones = \DB::table('asociaciones')->where('idE', '1');

        if (\Auth::user()->rol == 'dde') {

            return view('dde.integrantesasociacion', compact('Gastos'));

        }
        if (\Auth::user()->rol == 'rector') {

            return view('rector.integrantesasociacion', compact('id'));
        }
        if (\Auth::user()->rol == 'administrador') {

            return view('admin.integrantesasociacion', compact('id'));

        }
        if (\Auth::user()->rol == 'alumno') {

            return view('estudiantes.integrantesasociacion', compact('id'));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Asociaciones  $asociaciones
     * @return \Illuminate\Http\Response
     */
    public function edit(Asociaciones $asociaciones)
    {
        //
    }
    public function eliminarCuenta($id)
    {
        $cuentas = User::find($id);
        $cuentas->delete();
        $Asociaciones = \DB::table('users')->select('id', 'name', 'email', 'rol')->get();
        return view('admin.vercuentas', compact('Asociaciones'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Asociaciones  $asociaciones
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Asociaciones $asociaciones)
    {

        $aso = Asociaciones::find($request->id);
        $aso->update([
            'nombre'      => $request->nombre,
            'descripcion' => $request->des,
            'objetivo'    => $request->objetivo,
        ]);
        return response()->json([
            'mensaje' => $aso->nombre . ' fue editado correctamente',
        ]);
    }
    public function update1(Request $request)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Asociaciones  $asociaciones
     * @return \Illuminate\Http\Response
     */
    public function destroy(Asociaciones $asociaciones)
    {
        $producto = Asociaciones::find($id);
        $producto->delete();

        return response()->json([
            'mensaje' => $producto->descripcion . ' fue eliminado correctamente',
        ]);}
}
