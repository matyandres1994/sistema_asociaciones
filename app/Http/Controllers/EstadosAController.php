<?php

namespace App\Http\Controllers;

use App\EstadosA;
use Illuminate\Http\Request;

class EstadosAController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EstadosA  $estadosA
     * @return \Illuminate\Http\Response
     */
    public function show(EstadosA $estadosA)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EstadosA  $estadosA
     * @return \Illuminate\Http\Response
     */
    public function edit(EstadosA $estadosA)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EstadosA  $estadosA
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EstadosA $estadosA)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EstadosA  $estadosA
     * @return \Illuminate\Http\Response
     */
    public function destroy(EstadosA $estadosA)
    {
        //
    }
}
