<?php

namespace App\Http\Controllers;

use App\Tienen;
use Illuminate\Http\Request;

class TienenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tienen  $tienen
     * @return \Illuminate\Http\Response
     */
    public function show(Tienen $tienen)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tienen  $tienen
     * @return \Illuminate\Http\Response
     */
    public function edit(Tienen $tienen)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tienen  $tienen
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tienen $tienen)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tienen  $tienen
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tienen $tienen)
    {
        //
    }
}
