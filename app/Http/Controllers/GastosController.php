<?php

namespace App\Http\Controllers;

use App\Gastos;
use Illuminate\Http\Request;

class GastosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $id = \Auth::user()->id;

        $asociaciones = \DB::table('asociaciones')->join('tienens', 'asociaciones.id', '=', 'tienens.idA')->join('estadosA', 'asociaciones.idE', '=', 'estadosA.id')->select('asociaciones.id', 'nombre', 'descripcion')->where('idU', $id)->where('nombreE', '=', 'Aceptada')->get();

        if (\Auth::user()->rol == 'dde') {
            return view('dde.finanza', compact('asociaciones'));
        }
        if (\Auth::user()->rol == 'administrador') {
            return view('admin.finanza')->with('asociaciones', $asociaciones);
        }
        if (\Auth::user()->rol == 'alumno') {
            return view('estudiantes.finanza')->with('asociaciones', $asociaciones);
        }
        return "ERROR 404 PAGE NOT FOUNT";

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contar = \DB::table('tienens')->select(\DB::raw('COUNT(*) as idUA'))->where('tienens.idA', $request->idA)->where('tienens.idU', \Auth::user()->id)->where('tienens.idC', '1')->get()->first();
        if ($contar->idUA == 0) {
            return response()->json([
                'mensaje' => ' NO TIENES LOS PRIVILEGIOS SUFICIENTES PARA AGREGAR EN ESTA ASOCIACION',
            ]);
        }
        $producto = new Gastos($request->all());
        $name     = $request->file('archivo')->getClientOriginalName();
        $request->file('archivo')->move('../uploads', $name);
        $producto->archivo = $name;

        $producto->save();
        return response()->json([
            'mensaje' => $producto->descripcion . ' fue agregado correctamente',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Gastos  $gastos
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $idU = \Auth::user()->id;

        $contar = \DB::table('tienens')->select(\DB::raw('COUNT(tienens.idU) as idUA'))->join('asociaciones', 'tienens.idA', '=', 'asociaciones.id')->where('tienens.idA', $id)->where('asociaciones.idE', '1')->where('tienens.idU', \Auth::user()->id)->get()->first();
        if ($contar->idUA == 0) {
            return "ERROR 404 PAGE NOT FOUNT";
        } else {
            if (\Auth::user()->rol == 'dde') {
                $Gastos = \DB::table('gastos')->join('tienens', 'gastos.idA', '=', 'tienens.idA')->where('gastos.idA', $id)->where('idU', \Auth::user()->id)->get();
                return view('dde.DetallesGastos', compact('id'));

            }
            if (\Auth::user()->rol == 'administrador') {
                $Gastos = \DB::table('gastos')->join('tienens', 'gastos.idA', '=', 'tienens.idA')->where('gastos.idA', $id)->where('idU', \Auth::user()->id)->get();
                return view('admin.DetallesGastos', compact('id'));

            }
            if (\Auth::user()->rol == 'alumno') {
                $Gastos = \DB::table('gastos')->join('tienens', 'gastos.idA', '=', 'tienens.idA')->where('gastos.idA', $id)->where('idU', \Auth::user()->id)->get();
                return view('estudiantes.DetallesGastos', compact('id'));
            }}

    }
    public function lista($id)
    {
        $Gastos = \DB::table('gastos')->join('tienens', 'gastos.idA', '=', 'tienens.idA')->where('gastos.idA', $id)->where('idU', \Auth::user()->id)->get();

        return view('admin.tablaGastos', compact('Gastos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Gastos  $gastos
     * @return \Illuminate\Http\Response
     */
    public function edit(Gastos $gastos)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Gastos  $gastos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $producto = Gastos::find($id);
        $contar   = \DB::table('tienens')->select(\DB::raw('COUNT(*) as idUA'))->where('tienens.idA', $producto->idA)->where('tienens.idU', \Auth::user()->id)->where('tienens.idC', '1')->get()->first();
        if ($contar->idUA == 0) {
            return response()->json([
                'mensaje' => ' NO TIENES LOS PRIVILEGIOS SUFICIENTES PARA EDITAR EN ESTA ASOCIACION',
            ]);
        }
        $name = $request->file('archivoE')->getClientOriginalName();
        $request->file('archivoE')->move('../uploads', $name);
        $producto->update([
            'descripcion'    => $request->descripcionU,
            'montoUtilizado' => $request->montoUtilizadoU,
            'detalleBoleta'  => $request->detalleBoletaU,
            'fecha'          => $request->fechaU,
            'nombreE'        => $request->nombreEE,
            'rutE'           => $request->rutEE,
            'archivo'        => $name,
        ]);
        return response()->json([
            'mensaje' => $producto->descripcion . ' fue editado correctamente',
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Gastos  $gastos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gastos $gastos)
    {

    }

    public function archivo($file)
    {
        $pathtoFile = '../uploads/' . $file;
        return response()->download($pathtoFile);
    }

    public function destroyGasto(Gastos $gastos, $id)
    {

        $producto = Gastos::find($id);
        $contar   = \DB::table('tienens')->select(\DB::raw('COUNT(*) as idUA'))->where('tienens.idA', $producto->idA)->where('tienens.idU', \Auth::user()->id)->where('tienens.idC', '1')->get()->first();
        if ($contar->idUA == 0) {
            return response()->json([
                'mensaje' => ' NO TIENES LOS PRIVILEGIOS SUFICIENTES PARA ELIMINAR EN ESTA ASOCIACION',
            ]);
        }
        $producto->delete();
        return response()->json([
            'mensaje' => $producto->descripcion . ' fue eliminado correctamente',
        ]);

    }
}
