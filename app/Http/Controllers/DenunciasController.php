<?php

namespace App\Http\Controllers;

use App\Denuncias;
use Illuminate\Http\Request;

class DenunciasController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = \Auth::user()->id;

        $asociaciones = \DB::table('asociaciones')->join('estadosA', 'asociaciones.idE', '=', 'estadosA.id')->select('asociaciones.id', 'nombre', 'descripcion')->where('estadosA.id', '=', '1')->get();

        if (\Auth::user()->rol == 'dde') {
            return view('dde.denuncias', compact('asociaciones'));
        }
        if (\Auth::user()->rol == 'administrador') {
            return view('admin.denuncias', compact('asociaciones'));
        }
        if (\Auth::user()->rol == 'alumno') {
            return view('estudiantes.denuncias', compact('asociaciones'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $asociaciones = DB::table('asociaciones')->DB::insert(array(

            'detalleD' => $request->input('detalleD'),
            'fechaD'   => $request->input('fechaD'),
            'horaD'    => $request->input('horaD'),
            'ludarD'   => $request->input('ludarD'),

        ));

        return redirect()->action('ActividadesController@show');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $producto = Denuncias::create($request->all());
        return response()->json([
            'mensaje' => $producto->detalleD . ' fue ingresado correctamente',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Denuncias  $denuncias
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Actividades = \DB::table('denuncias')->join('asociaciones', 'denuncias.ida', '=', 'asociaciones.id')->get();

        if (\Auth::user()->rol == 'dde') {

            return view('dde.detalleDenuncia', compact('Actividades'));

        }
        if (\Auth::user()->rol == 'administrador') {

            return view('admin.detalleDenuncia', compact('denuncias', 'id'));

        }
        if (\Auth::user()->rol == 'alumno') {

            return view('estudiantes.detalleDenuncia', compact('Actividades', 'id'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {

        $actividad = Denuncias::find($id);
        if (\Auth::user()->rol != 'administrador') {
            return response()->json([
                'mensaje' => ' NO TIENES LOS PRIVILEGIOS SUFICIENTES PARA AGREGAR EN ESTA ASOCIACION',
            ]);
        }
        $actividad->update([
            'detalleD' => $request->detalle,
            'fechaD'   => $request->fecha,
            'horaD'    => $request->hora,
            'ludarD'   => $request->ludar,

        ]);
        return response()->json([
            'mensaje' => $actividad->detalleD . ' fue editado correctamente',
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function destroyDenuncias(Denuncias $Denuncias, $id)
    {

        $producto = Denuncias::find($id);
        if (\Auth::user()->rol != 'administrador') {
            return response()->json([
                'mensaje' => ' NO TIENES LOS PRIVILEGIOS SUFICIENTES PARA AGREGAR EN ESTA ASOCIACION',
            ]);
        }
        $producto->delete();

        return response()->json([
            'mensaje' => $producto->detalleD . 'fue eliminado correctamente',
        ]);

    }

    public function lista($id)
    {
        $Actividades = \DB::table('denuncias')->select('denuncias.id', 'detalleD', 'fechaD', 'horaD', 'ludarD')->join('asociaciones', 'asociaciones.id', '=', 'denuncias.idA')->where('asociaciones.id', $id)->get();

        return view('admin.tablaDenuncias', compact('Actividades'));
    }
}
