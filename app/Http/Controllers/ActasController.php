<?php

namespace App\Http\Controllers;

use App\Actas;
use App\Asociaciones;
use Illuminate\Http\Request;

class ActasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        if (\Auth::user()->rol == 'dde') {
            return view('dde.indexActas');
        }
        if (\Auth::user()->rol == 'administrador') {
            return view('admin.indexActas');
        }
        return "ERROR 404 PAGE NOT FOUNT";

    }
    public function archivo($file)
    {

        $pathtoFile = '../actas/' . $file;
        return response()->download($pathtoFile);
    }
    public function lista()
    {
        $asociaciones = \DB::table('asociaciones')->join('estadosA', 'asociaciones.idE', '=', 'estadosA.id')->select('asociaciones.id', 'nombre', 'descripcion')->where('estadosA.id', '=', '4')->get();
        return view('admin.tablaSolicitud', compact('asociaciones'));

    }
    public function pdf($id)
    {

        if (\Auth::user()->rol != 'dde' && \Auth::user()->rol != 'administrador') {
            return "No tiene los permisos necesarios para esta accion";
        }

        $integrantes = \DB::table('users')->join('tienens', 'users.id', '=', 'tienens.idU')->join('cargos', 'tienens.idC', '=', 'cargos.id')->select('name', 'email', 'cargos.nombre')->where('tienens.idA', '=', $id)->get();

        $asociacion = \DB::table('asociaciones')->where('id', '=', $id)->select('nombre', 'objetivo')->get();

        $producto = Asociaciones::find($id);
        $pdf      = \PDF::loadView('admin.pdf', ['integrantes' => $integrantes], ['asociacion' => $asociacion]);
        return $pdf->download('acta' . $producto->nombre . '.pdf');
    }

    public function info($id)
    {
        $integrantes = \DB::table('users')->join('tienens', 'users.id', '=', 'tienens.idU')->join('cargos', 'tienens.idC', '=', 'cargos.id')->select('name', 'email', 'cargos.nombre')->where('tienens.idA', '=', $id)->get();

        $asociacion = \DB::table('asociaciones')->where('id', '=', $id)->select('nombre', 'objetivo')->get();

        return view('admin.informacion', compact('integrantes', 'asociacion', 'id'));

    }
    public function actualizar(Request $request)
    {

        $act  = Actas::find($request->id);
        $name = $request->file('archivo')->getClientOriginalName();
        $request->file('archivo')->move('../actas', $name);
        $act->update([
            'archivo' => $name,
        ]);
        $aso = Asociaciones::find($request->idA);
        $aso->update([
            'idE' => $request->opcion,
        ]);
        return response()->json([
            'mensaje' => $act->descripcion . ' fue enviada correctamente',
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $producto = new Actas($request->all());
        $name     = $request->file('archivo')->getClientOriginalName();
        $request->file('archivo')->move('../actas', $name);
        $producto->archivo = $name;

        $producto->save();
        $aso = Asociaciones::find($producto->idA);
        $aso->update([
            'idE' => 5,
        ]);
        return response()->json([
            'mensaje' => $producto->descripcion . ' fue ingresado correctamente',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Actas  $actas
     * @return \Illuminate\Http\Response
     */
    public function show(Actas $actas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Actas  $actas
     * @return \Illuminate\Http\Response
     */
    public function edit(Actas $actas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Actas  $actas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Actas $actas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Actas  $actas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Actas $actas)
    {
        //
    }
}
