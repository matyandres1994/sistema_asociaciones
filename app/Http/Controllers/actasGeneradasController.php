<?php

namespace App\Http\Controllers;

use App\Actas;
use Illuminate\Http\Request;

class actasGeneradasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {

        if (\Auth::user()->rol == 'rector') {
            return view('rector.ActasGeneradas');
        }
        if (\Auth::user()->rol == 'administrador') {
            return view('admin.ActasGeneradas');
        } else {
            return "ERROR 404 PAGE NOT FOUNT";
        }

    }
    public function lista()
    {
        $actas = \DB::table('actas')->join('asociaciones', 'actas.idA', '=', 'asociaciones.id')->where('idE', 5)->select('actas.id', 'actas.idA', 'actas.descripcion', 'actas.comentario', 'actas.archivo')->get();
        return view('admin.tablaAG', compact('actas'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $act  = Actas::find($id);
        $name = $request->file('archivo')->getClientOriginalName();
        $request->file('archivo')->move('../actas', $name);
        return $name;
        $act->update([
            'archivo' => $name,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
