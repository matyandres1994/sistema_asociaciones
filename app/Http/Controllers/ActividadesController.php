<?php
namespace App\Http\Controllers;

use App\Actividades;
use Illuminate\Http\Request;

class ActividadesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
/**
 * Display a listing of the resource.
 *
 * @return \Illuminate\Http\Response
 */
    public function index()
    {
        $id = \Auth::user()->id;

        if (\Auth::user()->rol == 'dde') {
            $asociaciones = \DB::table('asociaciones')->join('tienens', 'id', '=', 'tienens.idA')->join('estadosA', 'asociaciones.idE', '=', 'estadosA.id')->where('nombreE', '=', 'Aceptada')->select('asociaciones.id', 'asociaciones.nombre', 'asociaciones.descripcion')->where('idU', $id)->get();
            return view('dde.actividades', compact('asociaciones'));
        }
        if (\Auth::user()->rol == 'administrador') {
            $asociaciones = \DB::table('asociaciones')->join('estadosA', 'asociaciones.idE', '=', 'estadosA.id')->where('nombreE', '=', 'Aceptada')->select('asociaciones.id', 'asociaciones.nombre', 'asociaciones.descripcion')->get();
            return view('admin.actividades', compact('asociaciones'));
        }
        if (\Auth::user()->rol == 'alumno') {
            $asociaciones = \DB::table('asociaciones')->join('tienens', 'id', '=', 'tienens.idA')->join('estadosA', 'asociaciones.idE', '=', 'estadosA.id')->where('nombreE', '=', 'Aceptada')->select('asociaciones.id', 'asociaciones.nombre', 'asociaciones.descripcion')->where('idU', $id)->get();
            return view('estudiantes.actividades', compact('asociaciones'));
        }
    }
/**
 * Show the form for creating a new resource.
 *
 * @return \Illuminate\Http\Response
 */
    public function create(Request $request)
    {

        $asociaciones = DB::table('asociaciones')->DB::insert(array(
            'nombreA'      => $request->input('nombreA'),
            'objetivoA'    => $request->input('objetivoA'),
            'fechaA'       => $request->input('fechaA'),
            'horaA'        => $request->input('horaA'),
            'lugarA'       => $request->input('lugarA'),
            'descripcionA' => $request->input('descripcionA'),
        ));
        return redirect()->action('ActividadesController@show');
    }
/**
 * Store a newly created resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @return \Illuminate\Http\Response
 */
    public function store(Request $request)
    {
        $contar = \DB::table('tienens')->select(\DB::raw('COUNT(*) as idUA'))->where('tienens.idA', $request->idA)->where('tienens.idU', \Auth::user()->id)->where('tienens.idC', '1')->get()->first();
        if ($contar->idUA == 0) {
            return response()->json([
                'mensaje' => ' NO TIENES LOS PRIVILEGIOS SUFICIENTES PARA AGREGAR EN ESTA ASOCIACION',
            ]);
        }

        $producto = Actividades::create($request->all());
        return response()->json([
            'mensaje' => $producto->detalleD . ' fue ingresado correctamente',
        ]);
    }
/**
 * Display the specified resource.
 *
 * @param  \App\Actividades  $actividades
 * @return \Illuminate\Http\Response
 */
    public function show($id)
    {
        $idU = \Auth::user()->id;

        $contar = \DB::table('tienens')->select(\DB::raw('COUNT(tienens.idU) as idUA'))->join('asociaciones', 'tienens.idA', '=', 'asociaciones.id')->where('tienens.idA', $id)->where('asociaciones.idE', '1')->where('tienens.idU', \Auth::user()->id)->get()->first();
        if (\Auth::user()->rol != 'administrador') {
            if ($contar->idUA == 0) {
                return "ERROR 404 PAGE NOT FOUNT";
            }
        }

        if (\Auth::user()->rol == 'dde') {
            $Actividades = \DB::table('actividades')->join('tienens', 'actividades.idA', '=', 'tienens.idA')->where('actividades.idA', $id)->get();
            return view('dde.detalleActividad', compact('Actividades', 'id'));
        }
        if (\Auth::user()->rol == 'administrador') {
            $Actividades = \DB::table('actividades')->join('tienens', 'actividades.idA', '=', 'tienens.idA')->where('actividades.idA', $id)->get();
            return view('admin.detalleActividad', compact('Actividades', 'id'));
        }
        if (\Auth::user()->rol == 'alumno') {
            $Actividades = \DB::table('actividades')->join('tienens', 'actividades.idA', '=', 'tienens.idA')->where('actividades.idA', $id)->get();
            return view('estudiantes.detalleActividad', compact('Actividades', 'id'));
        }
    }
/**
 * Show the form for editing the specified resource.
 *
 * @param  \App\Actividades  $actividades
 * @return \Illuminate\Http\Response
 */
    public function edit(Actividades $actividades)
    {
//
    }
    public function lista($id)
    {

        if (\Auth::user()->rol == 'dde') {
            $Actividades = \DB::table('actividades')->join('tienens', 'actividades.idA', '=', 'tienens.idA')->where('actividades.idA', $id)->where('idU', \Auth::user()->id)->get();
            return view('dde.tablaActividades', compact('Actividades'));
        }
        if (\Auth::user()->rol == 'administrador') {
            $Actividades = \DB::table('actividades')->where('actividades.idA', $id)->get();
            return view('admin.tablaActividades', compact('Actividades', 'id'));
        }
        if (\Auth::user()->rol == 'alumno') {
            $Actividades = \DB::table('actividades')->join('tienens', 'actividades.idA', '=', 'tienens.idA')->where('actividades.idA', $id)->where('idU', \Auth::user()->id)->get();
            return view('estudiantes.tablaActividades', compact('Actividades', 'id'));
        }
    }
/**
 * Update the specified resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @param  \App\Actividades  $actividades
 * @return \Illuminate\Http\Response
 */
/**
 * Remove the specified resource from storage.
 *
 * @param  \App\Actividades  $actividades
 * @return \Illuminate\Http\Response
 */
    public function destroy(Actividades $actividades)
    {
//
    }
    public function destroyActividad(Actividades $Actividades, $id)
    {
        $producto = Actividades::find($id);
        if (\Auth::user()->rol != 'administrador') {
            $contar = \DB::table('tienens')->select(\DB::raw('COUNT(*) as idUA'))->where('tienens.idA', $producto->idA)->where('tienens.idU', \Auth::user()->id)->where('tienens.idC', '1')->get()->first();
            if ($contar->idUA == 0) {
                return response()->json([
                    'mensaje' => ' NO TIENES LOS PRIVILEGIOS SUFICIENTES PARA AGREGAR EN ESTA ASOCIACION',
                ]);
            }
        }

        $producto->delete();
        return response()->json([
            'mensaje' => $producto->descripcion . ' fue eliminado correctamente',
        ]);

    }

    public function update(Request $request, $id)
    {

        $actividad = Actividades::find($id);

        $contar = \DB::table('tienens')->select(\DB::raw('COUNT(*) as idUA'))->where('tienens.idA', $actividad->idA)->where('tienens.idU', \Auth::user()->id)->where('tienens.idC', '1')->get()->first();
        if ($contar->idUA == 0) {
            return response()->json([
                'mensaje' => ' NO TIENES LOS PRIVILEGIOS SUFICIENTES PARA AGREGAR EN ESTA ASOCIACION',
            ]);
        }
        $actividad->update([
            'nombreA'      => $request->nombreAE,
            'objetivoA'    => $request->objetivoAE,
            'fechaA'       => $request->fechaAE,
            'horaA'        => $request->horaAE,
            'lugarA'       => $request->lugarAE,
            'descripcionA' => $request->descripcionAE,
        ]);
        return response()->json([
            'mensaje' => $actividad->nombreAE . ' fue editado correctamente',
        ]);
    }
    public function verGastos()
    {
        $asociaciones = \DB::table('asociaciones')->join('estadosA', 'asociaciones.idE', '=', 'estadosA.id')->where('nombreE', '=', 'Aceptada')->select('asociaciones.id', 'asociaciones.nombre', 'asociaciones.descripcion')->get();
        if (\Auth::user()->rol == 'administrador') {
            return view('admin.indexF', compact('asociaciones'));
        }
        if (\Auth::user()->rol == 'dde') {
            return view('dde.indexF', compact('asociaciones'));
        }
        return "NO TIENE ACCESO A ESTA RUTA";

    }

    public function listarAsociaciones()
    {
        $asociaciones = \DB::table('asociaciones')->join('estadosA', 'asociaciones.idE', '=', 'estadosA.id')->where('nombreE', '=', 'Aceptada')->select('asociaciones.id', 'asociaciones.nombre', 'asociaciones.descripcion')->get();
        if (\Auth::user()->rol == 'administrador') {

            return view('admin.tablaF', compact('asociaciones'));
        }
        if (\Auth::user()->rol == 'dde') {

            return view('admin.tablaF', compact('asociaciones'));
        }
        return "NO TIENE ACCESO A ESTA RUTA";
    }

    public function AsociacionesF($id, $fecha)
    {
        $fin    = $fecha . '-12-31';
        $inicio = $fecha . '-01-01';

        $asociaciones = \DB::table('gastos')->where('idA', $id)->whereBetween('fecha', array($inicio, $fin))->select('descripcion', 'montoUtilizado')->get();
        $total        = \DB::table('gastos')->where('idA', $id)->whereBetween('fecha', array($inicio, $fin))->select(\DB::raw('SUM(montoUtilizado) as total'))->get();
        if (\Auth::user()->rol == 'administrador') {
            return view('admin.tablaDF', compact('asociaciones', 'total'));
        }
        if (\Auth::user()->rol == 'dde') {
            return view('admin.tablaDF', compact('asociaciones', 'total'));
        }
        return "NO TIENE ACCESO A ESTA RUTA";
    }
}
