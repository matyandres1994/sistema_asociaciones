<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actividades extends Model
{
    protected $fillable = [
        'nombreA', 'objetivoA', 'fechaA','horaA','lugarA','descripcionA','idA'
    ];
}
