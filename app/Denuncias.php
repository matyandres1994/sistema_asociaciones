<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Denuncias extends Model
{
    protected $fillable = [
        'detalleD', 'fechaD', 'horaD', 'ludarD', 'idA',
    ];
}
