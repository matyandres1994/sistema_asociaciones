<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gastos extends Model
{
    //
    protected $fillable = [
        'id', 'descripcion', 'montoUtilizado', 'detalleBoleta', 'fecha', 'archivo', 'nombreE', 'rutE', 'idA',
    ];
}
