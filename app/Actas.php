<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actas extends Model
{
    //
    protected $fillable = [
        'descripcion', 'comentario', 'archivo','idA'
    ];
}
