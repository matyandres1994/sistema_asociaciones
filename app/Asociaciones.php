<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asociaciones extends Model
{
    //
    protected $fillable = [
        'nombre', 'descripcion', 'correo','objetivo','idE'
    ];
}
