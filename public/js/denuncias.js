var mostrarTabla = function() {
    var id = $('.tablad').attr('id');
    var url = $('.tablad').attr('url') + ":8000/denuncias/" + id;
    $.ajax({
        url: url,
        type: 'GET',
    }).done(function(data) {
        $('.tablad').empty().html(data);
        console.log("success");
        $('#btnIngresar').click(function(e) {
            e.preventDefault();
            $('#modalingresar').modal('show');
            $('#ingresar').unbind('click').click(function(e) {
                var detalleD = $('#detalleD').val();
                var fechaD = $('#fechaD').val();
                var horaD = $('#horaD').val();
                var ludarD = $('#ludarD').val();
                var idA = $('#idA').val();
                if (detalleD == '') {
                    $('#alerta1').show();
                    $('#alerta1').text("Rellene destalle");
                    return false;
                }
                if (fechaD == '') {
                    $('#alerta1').show();
                    $('#alerta1').text("Ingrese fecha");
                    return false;
                }
                if (horaD == '') {
                    $('#alerta1').show();
                    $('#alerta1').text("Establezca una hora");
                    return false;
                }
                if (ludarD == '') {
                    $('#alerta1').show();
                    $('#alerta1').text("Ingrese lugar");
                    return false;
                }
                $('#modalingresar').modal('hide');
                var url1 = $('#formIngresar').attr('action');
                $.ajax({
                    url: url1,
                    type: 'POST',
                    data: {
                        detalleD: detalleD,
                        fechaD: fechaD,
                        horaD: horaD,
                        ludarD: ludarD,
                        idA: idA,
                        _token: $('#signup-token').val()
                    },
                }).done(function(result) {
                    $('#alerta').show();
                    $('#alerta').html(result.mensaje);
                    mostrarTabla();
                    console.log(result);
                }).fail(function(result) {
                    console.log("error");
                })
            });
        });
        $('.btnEliminar').unbind('click').click(function(e) {
            e.preventDefault();
            if (!confirm("¿Seguro que desea eliminar el producto?")) {
                return false;
            }
            var row = $(this).parents('tr');
            var id = row.attr('id');
            var urlEliminar = $('.tablad').attr('url') + ":8000/eliminard/" + id;
            $.ajax({
                url: urlEliminar,
                type: 'GET',
            }).done(function(result) {
                row.fadeOut();
                $('#alerta').show();
                $('#alerta').html(result.mensaje);
                console.log("success");
                mostrarTabla();
            }).fail(function(result) {
                console.log("error");
                alert(result.status);
                mostrarTabla();
            });
        });
        $('.btnEdit').click(function(e) {
            e.preventDefault();
            var row = $(this).parents('tr');
            var id = row.attr('id');
            var urleditar = $('.tablad').attr('url') + ":8000/editard/" + id;
            $('#modaleditar').modal('show');
            var detalleD = row.find("th")[0].innerHTML;
            var fechaD = row.find("th")[1].innerHTML;
            var horaD = row.find("th")[2].innerHTML;
            var ludarD = row.find("th")[3].innerHTML;
            $('#detalle').val(detalleD);
            $('#fecha').val(fechaD);
            $('#hora').val(horaD);
            $('#ludar').val(ludarD);
            $('.btnEditar').unbind('click').click(function(e) {
                e.preventDefault();
                var no = $('#detalle').val();
                var ob = $('#fecha').val();
                var fe = $('#hora').val();
                var ho = $('#ludar').val();
                if (no == '') {
                    $('#alerta2').show();
                    $('#alerta2').text("Rellene el Nombre");
                    return false;
                }
                if (ob == '') {
                    $('#alerta2').show();
                    $('#alerta2').text("Rellene el Objetivo");
                    return false;
                }
                if (fe == '') {
                    $('#alerta2').show();
                    $('#alerta2').text("Establezca una fecha");
                    return false;
                }
                if (ho == '') {
                    $('#alerta2').show();
                    $('#alerta2').text("Establezca una hora");
                    return false;
                }
                $('#modaleditar').modal('hide');
                $.ajax({
                    url: urleditar,
                    type: 'put',
                    data: {
                        detalle: no,
                        fecha: ob,
                        hora: fe,
                        ludar: ho,
                        _token: $('#signup-token').val()
                    },
                }).done(function(result) {
                    $('#alerta').show();
                    $('#alerta').html(result.mensaje);
                    mostrarTabla();
                    console.log(result);
                }).fail(function(result) {
                    console.log("error");
                })
            });
        });
    });
}
$(document).ready(function() {
    mostrarTabla();
    $('#alerta1').hide();
    $('#alerta2').hide();
    mostrarTabla();
});