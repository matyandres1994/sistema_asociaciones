$(document).ready(function() {
    lista();
    $('.loader').show();
});
var lista = function() {
    var url = $('#tablaSolicitudes').attr('url') + ":8000/s1o1l1i1c1i1t1u1d1e1s";
    $.ajax({
        url: url,
        type: 'GET',
    }).done(function(data) {
        $('#tablaSolicitudes').empty().html(data);
        $('.loader').hide();
        $('.btnDetalles').click(function(e) {
            e.preventDefault();
            $('#modaleditar').modal('show');
            var row = $(this).parents('tr');
            var id = row.attr('id');
            var urlInfo = $('#tablaSolicitudes').attr('url') + ":8000/i1n1f1o/" + id;
            $('.loader').show();
            $.ajax({
                url: urlInfo,
                type: 'GET',
            }).done(function(data) {
                $('#info').empty().html(data);
                $('.loader').hide();
                $('#btnGenerar').click(function(e) {
                    $('#modaleditar').modal('hide');
                    $('#modalFormulario').modal('show');
                    $('#btnEA').unbind('click').click(function(e) {
                        e.preventDefault();
                        if ($('#descripcion').val() == '') {
                            toastr.remove();
                            toastr.warning('Campo "descripcion" vacio');
                            return false;
                        }
                        if ($('#comentario').val() == '') {
                            toastr.remove();
                            toastr.warning('Campo "comentario" vacio');
                            return false;
                        }
                        if ($('#archivo').val() == '') {
                            toastr.remove();
                            toastr.warning('Campo "archivo" vacio');
                            return false;
                        }
                        $('#modalFormulario').modal('hide');
                        var url1 = $('#formularioActa').attr('action');
                        var formData = new FormData(document.getElementById("formularioActa"));
                        formData.append("dato", "valor");
                        formData.append("idA", id);
                        $('.loader').show();
                        $.ajax({
                            url: url1,
                            type: 'POST',
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false
                        }).done(function(res) {
                            $('.loader').hide();
                            toastr.remove();
                            toastr.info(res.mensaje);
                            lista();
                        }).fail(function() {
                            console.log("error");
                        }).always(function() {
                            console.log("complete");
                        });
                    });
                })
            }).fail(function() {
                console.log("error");
            }).always(function() {
                console.log("complete");
            });
        })
    }).fail(function() {
        console.log("error");
    });
}

function fileValidation() {
    var fileInput = document.getElementById('archivo');
    var filePath = fileInput.value;
    var allowedExtensions = /(.pdf)$/i;
    if (!allowedExtensions.exec(filePath)) {
        alert('Solo se aceptan archivos pdf.');
        fileInput.value = '';
        return false;
    }
    var tama = fileInput.files[0].size
    if (tama > 3000000) {
        alert("El archivo no puede superar los 3mb");
        fileInput.value = '';
        return false;
    }
}