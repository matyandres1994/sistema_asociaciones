var gastos = function() {
    var url = $('#tablaF').attr('url');
    $.ajax({
        url: url + ":8000/asociaciones",
        type: 'GET',
    }).done(function(res) {
        console.log("success");
        $('#tablaF').empty().html(res);
        $('.btnVer').click(function(e) {
            var id = $(this).parents('tr').attr('id');
            e.preventDefault();
            $('#modalVer').modal('show');
            $('#ingresar').unbind('click').click(function(e) {
                e.preventDefault();
                ano = $('#fecha').val();
                urlInfo = url + ":8000/AsociacionesF/" + id + "/" + ano;
                $.ajax({
                    url: urlInfo,
                    type: 'GET',
                }).done(function(ress) {
                    $('#datos').empty().html(ress);
                }).fail(function() {
                    console.log("error");
                }).always(function() {
                    console.log("complete");
                });
            });
        });
    }).fail(function() {
        console.log("error");
    }).always(function() {
        console.log("complete");
    });
}
$(document).ready(function() {
    gastos();
});