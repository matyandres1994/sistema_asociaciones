$(document).ready(function() {
    lista();
    $('#alerta').hide();
    $('#alerta1').hide();
    $('#alerta2').hide();
});
var lista = function() {
    APP_URL = $('#formeditar').attr('url');
    var urltabla = APP_URL + ':8000/listaAsociaciones';
    $.ajax({
        url: urltabla,
        type: 'GET',
        dataType: 'html',
    }).done(function(data) {
        $('.tabla').empty().html(data);
        $('.eliminaras').unbind('click').click(function(e) {
            e.preventDefault();
            if (!confirm("¿Seguro que desea eliminar la asociación?")) {
                return false;
            }
            var row = $(this).parents('tr');
            var idEliminar = row.attr('id');
            var url = $('#tabla').attr('url');
            $.ajax({
                url: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'post',
                data: {
                    id: idEliminar
                },
            }).done(function() {
                lista();
            }).fail(function() {
                console.log("error");
            }).always(function() {
                console.log("complete");
                alert("Se ha eliminado");
            });
        })
        $('#botonagregar').click(function(e) {
            e.preventDefault();
            $('#modalingresar').modal('show');
            $('#ingresar').unbind('click').click(function() {
                e.preventDefault();
                var nombre = $('#nombre2').val();
                var descripcion = $('#descripcion2').val();
                var correo = $('#correo2').val();
                var objetivo = $('#objetivo2').val();
                var presidente = $('#presi').val();
                var secretario = $('#teso').val();
                var tesorero = $('#secre').val();
                if (nombre == '') {
                    $('#alerta1').show();
                    $('#alerta1').text("Debe ingresar un nombre");
                    return false;
                }
                if (descripcion == '') {
                    $('#alerta1').show();
                    $('#alerta1').text("Agregue una descripción");
                    return false;
                }
                if (correo == '') {
                    $('#alerta1').show();
                    $('#alerta1').text("Agregue un correo");
                    return false;
                }
                if (objetivo == '') {
                    $('#alerta1').show();
                    $('#alerta1').text("Agregue un objetivo");
                    return false;
                }
                if (presidente == 'Elegir') {
                    $('#alerta1').show();
                    $('#alerta1').text("Debe ingresar un presidente");
                    return false;
                }
                if (tesorero == 'Elegir') {
                    $('#alerta1').show();
                    $('#alerta1').text("Debe ingresar un tesorero");
                    return false;
                }
                if (secretario == 'Elegir') {
                    $('#alerta1').show();
                    $('#alerta1').text("Debe ingresar un secretario");
                    return false;
                }
                if (tesorero == presidente) {
                    alert("Debe elegir un TESORERO distinto a PRESIDENTE ");
                    return false;
                }
                if (tesorero == secretario) {
                    alert("Debe elegir un TESORERO distinto a SECRETARIO");
                    return false;
                }
                if (secretario == presidente) {
                    alert("Debe elegir un SECRETARIO distinto a PRESIDENTE ");
                    return false;
                }
                $('#modalingresar').modal('hide');
                var url1 = $('#formIngresar').attr('action');
                $.ajax({
                    url: url1,
                    type: 'POST',
                    data: {
                        nombre: nombre,
                        descripcion: descripcion,
                        correo: correo,
                        objetivo: objetivo,
                        presi: presidente,
                        teso: tesorero,
                        secre: secretario,
                        idE: 4,
                        _token: $('#signup-token').val()
                    },
                }).done(function(result) {
                    $('#nombre2').val('');
                    $('#descripcion2').val('');
                    $('#correo2').val('');
                    $('#objetivo2').val('');
                    $('#presi').val('');
                    $('#teso').val('');
                    $('#secre').val('');
                    $('#alerta').show();
                    $('#alerta').html(result.mensaje);
                    lista();
                    console.log("success");
                }).fail(function(result) {
                    console.log("error");
                });
            });
        });
        $('.botoneditar').click(function(e) {
            e.preventDefault();
            var row = $(this).parents('tr');
            var idEditar = row.attr('id');
            var urleditar = $('#formeditar').attr('url') + ":8000/editar1/" + idEditar;
            $('#modaleditar').modal('show');
            $('#alerta2').hide();
            var nombre = row.find("th")[0].innerHTML;
            var descripcion = row.find("th")[1].innerHTML;
            var detalles = row.find("th")[2].innerHTML;
            $('#nombre1').val(nombre);
            $('#descripcion1').val(descripcion);
            $('#detalles1').val(detalles);
            $('#btnedit').unbind('click').click(function(e) {
                e.preventDefault();
                var nombre1 = $('#nombre1').val();
                var descripcion1 = $('#descripcion1').val();
                var detalles1 = $('#detalles1').val();
                if (nombre1 == '') {
                    $('#alerta2').show();
                    $('#alerta2').text("Debe ingresar un nombre");
                    return false;
                }
                if (descripcion1 == '') {
                    $('#alerta2').show();
                    $('#alerta2').text("Rellene la descripción");
                    return false;
                }
                if (detalles1 == '') {
                    $('#alerta2').show();
                    $('#alerta2').text("Rellene el campo detalle");
                    return false;
                }
                $('#modaleditar').modal('hide');
                $.ajax({
                    url: urleditar,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'PUT',
                    data: {
                        id: idEditar,
                        nombre: nombre1,
                        des: descripcion1,
                        objetivo: detalles1
                    }
                }).done(function(result) {
                    $('#alerta').show();
                    $('#alerta').html(result.mensaje);
                    lista();
                    console.log(result);
                }).fail(function(result) {
                    console.log("error");
                })
            });
        });
    }).fail(function() {
        console.log("error");
    }).always(function() {
        console.log("complete");
    });
}