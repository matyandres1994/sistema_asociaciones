$(document).ready(function() {
    lista();
    $('.loader').show();
});
var lista = function() {
    var url = $('#tablasActas').attr('UrlNativo') + ":8000/actasGeneradas";
    $.ajax({
        url: url,
        type: 'GET',
    }).done(function(data) {
        $('#tablasActas').empty().html(data);
        $('.loader').hide();
        $('.btnIngresar').unbind('click').click(function(e) {
            e.preventDefault();
            var row = $(this).parents('tr');
            var id = row.attr('id');
            var idA = row.attr('idA');
            $('#modalACTA').modal('show');
            $('#btnEA').unbind('click').click(function(e) {
                if ($('input:radio[name=opcion]:checked').val() == null) {
                    toastr.remove();
                    toastr.warning('Seleccione una opcion');
                    return false;
                }
                if ($('#archivo').val() == '') {
                    toastr.remove();
                    toastr.warning('Campo "archivo" vacio');
                    return false;
                }
                $('#modalACTA').modal('hide');
                var url1 = $('#formularioActa').attr('action');
                var formData = new FormData(document.getElementById("formularioActa"));
                formData.append("dato", "valor");
                formData.append("idA", idA);
                formData.append("id", id);
                $('.loader').show();
                $.ajax({
                    url: url1,
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false
                }).done(function(res) {
                    console.log("xd");
                    toastr.remove();
                    $('.loader').hide();
                    toastr.info(res.mensaje);
                    lista();
                }).fail(function() {
                    console.log("error");
                }).always(function() {
                    console.log("complete");
                });
            })
        })
    }).fail(function() {
        console.log("error");
    }).always(function() {
        console.log("complete");
    });
}

function fileValidation() {
    var fileInput = document.getElementById('archivo');
    var filePath = fileInput.value;
    var allowedExtensions = /(.pdf)$/i;
    if (!allowedExtensions.exec(filePath)) {
        alert('Solo se aceptan archivos pdf.');
        fileInput.value = '';
        return false;
    }
    var tama = fileInput.files[0].size
    if (tama > 3000000) {
        alert("El archivo no puede superar los 3mb");
        fileInput.value = '';
        return false;
    }
}