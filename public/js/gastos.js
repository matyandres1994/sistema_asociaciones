var lista = function() {
    var row = $('.tabla-gastos').attr('id');
    var url = $('.tabla-gastos').attr('url') + row;
    $.ajax({
        type: 'GET',
        url: url,
        success: function(data) {
            $('.tabla-gastos').empty().html(data);
            $('.loader').hide();
            $('#btnIngresar').click(function(e) {
                e.preventDefault();
                $('#modalingresar').modal('show');
                $("#ingresar").unbind('click').click(function(e) {
                    e.preventDefault();
                    des = $('#descripcion').val();
                    montoUtilizado = $('#montoUtilizado').val();
                    detalleBoleta = $('#detalleBoleta').val();
                    fecha = $('#fecha').val();
                    archivo = $('#archivo').val();
                    rutE = $('#rutE').val();
                    nombreE = $('#nombreE').val();
                    if (des == '') {
                        toastr.remove();
                        toastr.warning('Campo "descripcion" vacio');
                        return false;
                    }
                    if (montoUtilizado == '') {
                        toastr.remove();
                        toastr.warning('Campo "monto utilizado" vacio');
                        return false;
                    }
                    if (detalleBoleta == '') {
                        toastr.remove();
                        toastr.warning('Campo "detalle de boleta" vacio');
                        return false;
                    }
                    if (fecha == '' || fecha < '2017-01-01') {
                        toastr.remove();
                        toastr.warning('Campo "fecha" vacio o ingrese una fecha valida');
                        return false;
                    }
                    if (archivo == '') {
                        toastr.remove();
                        toastr.warning('Campo "archivo" vacio');
                        return false;
                    }
                    if (nombreE == '') {
                        toastr.remove();
                        toastr.warning('Campo "nombre empresa" vacio');
                        return false;
                    }
                    if (rutE == '') {
                        toastr.remove();
                        toastr.warning('Campo "rut empresa" vacio');
                        return false;
                    }
                    var f = $(this);
                    var url1 = $('#formIngresar').attr('action');
                    var formData = new FormData(document.getElementById("formIngresar"));
                    formData.append("dato", "valor");
                    $('#descripcion').val('');
                    $('#montoUtilizado').val('');
                    $('#detalleBoleta').val('');
                    $('#fecha').val('');
                    $('#archivo').val('');
                    $('#nombreE').val('');
                    $('#rutE').val('');
                    //formData.append(f.attr("name"), $(this)[0].files[0]);
                    $('.loader').show();
                    $.ajax({
                        url: url1,
                        type: "POST",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false
                    }).done(function(res) {
                        $('#modalingresar').modal('hide');
                        $('.loader').hide();
                        toastr.remove();
                        toastr.info(res.mensaje);
                        console.log(res);
                        lista();
                    });
                });
            });
            $('.btnEliminar').unbind('click').click(function(e) {
                e.preventDefault();
                if (!confirm("¿Seguro que desea eliminar el producto?")) {
                    return false;
                }
                var row = $(this).parents('tr');
                var idx = row.attr('id');
                var urlEliminar = $('#Eliminarbtn').attr('url') + idx;
                $('.loader').show();
                $.ajax({
                    url: urlEliminar,
                    type: 'GET',
                }).done(function(result) {
                    row.fadeOut();
                    $('.loader').hide();
                    toastr.remove();
                    toastr.info(result.mensaje);
                    console.log("success");
                    lista();
                }).fail(function(result) {
                    console.log("error");
                    alert(result.status);
                });
            });
            $('.btnEdit').click(function(e) {
                e.preventDefault();
                var row = $(this).parents('tr');
                var id = row.attr('id');
                var urleditar = $('#Editarbtn').attr('url') + id;
                $('#modaleditar').modal('show');
                var descripcion = row.find("th")[0].innerHTML;
                var dUtilizado = row.find("th")[1].innerHTML;
                var dBoleta = row.find("th")[2].innerHTML;
                var fUtlizada = row.find("th")[3].innerHTML;
                var nombreEE = row.find("th")[5].innerHTML;
                var rutEE = row.find("th")[6].innerHTML;
                $('#descripcionU').val(descripcion);
                $('#montoUtilizadoU').val(dUtilizado);
                $('#detalleBoletaU').val(dBoleta);
                $('#fechaU').val(fUtlizada);
                $('#nombreEE').val(nombreEE);
                $('#rutEE').val(rutEE);
                $('.btnEditar').unbind('click').click(function(e) {
                    e.preventDefault();
                    var des = $('#descripcionU').val();
                    var mon = $('#montoUtilizadoU').val();
                    var det = $('#detalleBoletaU').val();
                    var fec = $('#fechaU').val();
                    rutEx = $('#rutEE').val();
                    nombreEx = $('#nombreEE').val();
                    archivoE = $('#archivoE').val();
                    if (des == '') {
                        toastr.remove();
                        toastr.warning('Campo "descripcion" vacio');
                        return false;
                    }
                    if (mon == '') {
                        toastr.remove();
                        toastr.warning('Campo "monto utilizado" vacio');
                        return false;
                    }
                    if (det == '') {
                        toastr.remove();
                        toastr.warning('Campo "detalle boleta" vacio');
                        return false;
                    }
                    if (fec == '' || fec < '2017-01-01') {
                        toastr.remove();
                        toastr.warning('Campo "fecha" vacio');
                        return false;
                    }
                    if (nombreEx == '') {
                        toastr.remove();
                        toastr.warning('Campo "nombre empresa" vacio');
                        return false;
                    }
                    if (rutEx == '') {
                        toastr.remove();
                        toastr.warning('Campo "rut empresa" vacio');
                        return false;
                    }
                    if (archivoE == '') {
                        toastr.remove();
                        toastr.warning('Campo "Archivo" vacio');
                        return false;
                    }
                    var formData1 = new FormData(document.getElementById("FEditar"));
                    formData1.append("dato", "valor");
                    $('.loader').show();
                    $.ajax({
                        url: urleditar,
                        type: 'POST',
                        data: formData1,
                        cache: false,
                        contentType: false,
                        processData: false
                    }).done(function(result) {
                        $('#modaleditar').modal('hide');
                        $('.loader').hide();
                        toastr.remove();
                        toastr.info(result.mensaje);
                        lista();
                        console.log(result);
                    }).fail(function(result) {
                        console.log("error");
                    })
                });
            });
        }
    });
}
$(document).ready(function() {
    lista();
    $('.loader').show();
});

function fileValidation() {
    var fileInput = document.getElementById('archivo');
    var filePath = fileInput.value;
    var allowedExtensions = /(.pdf)$/i;
    if (!allowedExtensions.exec(filePath)) {
        toastr.error('Solo se aceptan archivos pdf.');
        fileInput.value = '';
        return false;
    }
    var tama = fileInput.files[0].size
    if (tama > 3000000) {
        toastr.error("El archivo no puede superar los 3mb");
        fileInput.value = '';
        return false;
    }
    var tama = fileInput.files[0].size
    if (tama > 3000000) {
        toastr.error("El archivo no puede superar los 3mb");
        fileInput.value = '';
        return false;
    }
}

function validaRut(Objeto) {
    var tmpstr = "";
    var intlargo = Objeto.value
    if (intlargo.length > 0) {
        crut = Objeto.value
        largo = crut.length;
        if (largo < 2) {
            toastr.error('El rut ingresado es invalido');
            Objeto.focus();
            return false;
        }
        for (i = 0; i < crut.length; i++)
            if (crut.charAt(i) != ' ' && crut.charAt(i) != '.' && crut.charAt(i) != '-') {
                tmpstr = tmpstr + crut.charAt(i);
            }
        rut = tmpstr;
        crut = tmpstr;
        largo = crut.length;
        if (largo > 2) rut = crut.substring(0, largo - 1);
        else rut = crut.charAt(0);
        dv = crut.charAt(largo - 1);
        if (rut == null || dv == null) return 0;
        var dvr = '0';
        suma = 0;
        mul = 2;
        for (i = rut.length - 1; i >= 0; i--) {
            suma = suma + rut.charAt(i) * mul;
            if (mul == 7) mul = 2;
            else mul++;
        }
        res = suma % 11;
        if (res == 1) dvr = 'k';
        else if (res == 0) dvr = '0';
        else {
            dvi = 11 - res;
            dvr = dvi + "";
        }
        if (dvr != dv.toLowerCase()) {
            toastr.error('El rut ingresado es invalido');
            // Objeto.style.backgroundColor = '#F18C8C';
            Objeto.value = "";
            Objeto.focus();
            return false;
        }
        //Objeto.style.backgroundColor = '#6FD84C';
        Objeto.focus()
        return true;
    }
}

function archivo() {
    var fileInput = document.getElementById('archivoE');
    var filePath = fileInput.value;
    var allowedExtensions = /(.pdf)$/i;
    if (!allowedExtensions.exec(filePath)) {
        toastr.error('Solo se aceptan archivos pdf.');
        fileInput.value = '';
        return false;
    }
    var tama = fileInput.files[0].size
    if (tama > 3000000) {
        toastr.error("El archivo no puede superar los 3mb");
        fileInput.value = '';
        return false;
    }
    var tama = fileInput.files[0].size
    if (tama > 3000000) {
        toastr.error("El archivo no puede superar los 3mb");
        fileInput.value = '';
        return false;
    }
}