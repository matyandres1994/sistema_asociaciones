var mostrarTabla = function() {
    var id = $('.tablaA').attr('id');
    var url = $('.tablaA').attr('url') + ":8000/actividades/" + id;
    $.ajax({
        url: url,
        type: 'GET',
    }).done(function(data) {
        $('.tablaA').empty().html(data);
        console.log("success");
        $('#btnIngresar').click(function(e) {
            e.preventDefault();
            $('#modalingresar').modal('show');
            $('#ingresar').unbind('click').click(function(e) {
                var nombreA = $('#nombreA').val();
                var objetivoA = $('#objetivoA').val();
                var fechaA = $('#fechaA').val();
                var horaA = $('#horaA').val();
                var lugarA = $('#lugarA').val();
                var descripcionA = $('#descripcionA').val();
                var idA = $('#idA').val();
                actual = new Date();
                ingresada = new Date(fechaA);
                if (nombreA == '') {
                    $('#alerta1').show();
                    $('#alerta1').text("Rellene el Nombre");
                    return false;
                }
                if (objetivoA == '') {
                    $('#alerta1').show();
                    $('#alerta1').text("Rellene el Objetivo");
                    return false;
                }
                if (ingresada < actual) {
                    $('#alerta1').show();
                    $('#alerta1').text("Establezca una fecha posterior");
                    return false;
                }
                if (fechaA == '') {
                    $('#alerta1').show();
                    $('#alerta1').text("Establezca una fecha");
                    return false;
                }
                if (horaA == '') {
                    $('#alerta1').show();
                    $('#alerta1').text("Establezca una hora");
                    return false;
                }
                if (lugarA == '') {
                    $('#alerta1').show();
                    $('#alerta1').text("Establezca un lugar");
                    return false;
                }
                if (descripcionA == '') {
                    $('#alerta1').show();
                    $('#alerta1').text("Establezca una descripcion");
                    return false;
                }
                $('#modalingresar').modal('hide');
                var url1 = $('#formIngresar').attr('action');
                $.ajax({
                    url: url1,
                    type: 'POST',
                    data: {
                        nombreA: nombreA,
                        objetivoA: objetivoA,
                        fechaA: fechaA,
                        horaA: horaA,
                        lugarA: lugarA,
                        descripcionA: descripcionA,
                        idA: idA,
                        _token: $('#signup-token').val()
                    },
                }).done(function(result) {
                    $('#alerta').show();
                    $('#alerta').html(result.mensaje);
                    mostrarTabla();
                    console.log(result);
                }).fail(function(result) {
                    console.log("error");
                })
            });
        });
        $('.btnEliminar').unbind('click').click(function(e) {
            e.preventDefault();
            if (!confirm("¿Seguro que desea eliminar esta actividad?")) {
                return false;
            }
            var row = $(this).parents('tr');
            var id = row.attr('id');
            var urlEliminar = $('.tablaA').attr('url') + ':8000/eliminarA/' + id;
            $.ajax({
                url: urlEliminar,
                type: 'GET',
            }).done(function(result) {
                row.fadeOut();
                $('#alerta').show();
                $('#alerta').html(result.mensaje);
                console.log("success");
                mostrarTabla();
            }).fail(function(result) {
                console.log("error");
                alert(result.status);
                mostrarTabla();
            });
        });
        $('.btnEdit').click(function(e) {
            e.preventDefault();
            var row = $(this).parents('tr');
            var id = row.attr('id');
            var urleditar = $('.tablaA').attr('url') + ':8000/editarA/' + id;
            $('#modaleditar').modal('show');
            var nomb = row.find("th")[0].innerHTML;
            var obj = row.find("th")[1].innerHTML;
            var fech = row.find("th")[2].innerHTML;
            var hor = row.find("th")[3].innerHTML;
            var lug = row.find("th")[4].innerHTML;
            var desc = row.find("th")[5].innerHTML;
            $('#nombreAE').val(nomb);
            $('#objetivoAE').val(obj);
            $('#fechaAE').val(fech);
            $('#horaAE').val(hor);
            $('#lugarAE').val(lug);
            $('#descripcionAE').val(desc);
            $('.btnEditar').unbind('click').click(function(e) {
                e.preventDefault();
                var no = $('#nombreAE').val();
                var ob = $('#objetivoAE').val();
                var fe = $('#fechaAE').val();
                var ho = $('#horaAE').val();
                var lu = $('#lugarAE').val();
                var de = $('#descripcionAE').val();
                if (no == '') {
                    $('#alerta2').show();
                    $('#alerta2').text("Rellene el Nombre");
                    return false;
                }
                if (ob == '') {
                    $('#alerta2').show();
                    $('#alerta2').text("Rellene el Objetivo");
                    return false;
                }
                if (fe == '') {
                    $('#alerta2').show();
                    $('#alerta2').text("Establezca una fecha");
                    return false;
                }
                if (ho == '') {
                    $('#alerta2').show();
                    $('#alerta2').text("Establezca una hora");
                    return false;
                }
                if (lu == '') {
                    $('#alerta2').show();
                    $('#alerta2').text("Establezca un lugar");
                    return false;
                }
                if (de == '') {
                    $('#alerta2').show();
                    $('#alerta2').text("Establezca una descripcion");
                    return false;
                }
                $('#modaleditar').modal('hide');
                $.ajax({
                    url: urleditar,
                    type: 'put',
                    data: {
                        nombreAE: no,
                        objetivoAE: ob,
                        fechaAE: fe,
                        horaAE: ho,
                        lugarAE: lu,
                        descripcionAE: de,
                        _token: $('#signup-token').val()
                    },
                }).done(function(result) {
                    $('#alerta').show();
                    $('#alerta').html(result.mensaje);
                    mostrarTabla();
                    console.log(result);
                }).fail(function(result) {
                    console.log("error");
                })
            });
        });
    });
}
$(document).ready(function() {
    mostrarTabla();
    $('#alerta1').hide();
    $('#alerta2').hide();
    mostrarTabla();
});