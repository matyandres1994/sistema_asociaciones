@extends('layouts.rector')
@section('meta')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
<h2 class="panel-body text-center" style="color:#333390">Rendir gastos </h2>
<div class="gastos">
    
    <div class="tabla-gastos" id="{{ $id }}" url="{{ env('APP_URL') }}:8000/lista/">
    </div>
    
    <div class="loader" ></div>
    <!-- modal ingresar -->
    <div id="modalingresar" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content " style="filter:alpha(opacity=50); opacity:0.9;">
                <div class="container">
                    <form id="formIngresar" action="{{ route('Finanzas.store') }}" method="POST">
                        {{ csrf_field() }}                              <br>
                        <h4 style="text-align: center; color: #3F78BF">Insertar datos</h4>
                        <br/>
                        
                        <label>Descripción:</label>
                        <input type="text" required="" name="descripcion" placeholder="Descripcion"  id="descripcion" class="form-control"/>
                        <label>Dinero utilizado:</label>
                        <input type="number" required="" placeholder="Monto utilizado" name="montoUtilizado" id="montoUtilizado" class="form-control" />
                        <label>Detalle de boleta:</label>
                        <input type="text" required="" placeholder="Detalle boleta" name="detalleBoleta" id="detalleBoleta" class="form-control" />
                        <label>Fecha:</label>
                        <input type="date" required="" placeholder="Fecha del gasto" min="2017-01-01" name="fecha" id="fecha" class="form-control" />
                        
                        <label>archivo:</label>
                        <input type="file" required="" name="archivo" id="archivo" class="form-control" onchange="return fileValidation()" />
                        <label>Nombre empresa:</label>
                        <input type="text" required="" name="nombreE" placeholder="Nombre Empresa"  id="nombreE" class="form-control"/>
                        <label>Rut empresa:</label>
                        <input type="text" name="rutE" id="rutE" placeholder="rut Empresa" class="form-control" onchange="validaRut(this)" required="">
                        <input type="hidden" id="idA" value="{{ $id }}" name="idA" />
                        <input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}" />
                        
                        <br/>
                        <button   class="btn btn-info form-control ingresar" id="ingresar"> Ingresar</button>
                        <br>
                        
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- modal editar -->
    <div id="modaleditar" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content " style="filter:alpha(opacity=50); opacity:0.9;">
                <div class="container">
                    <form id="FEditar" action="  " method="POST">
                         {{ csrf_field() }} 
                        <br>
                        <h4 style="text-align: center; color: #3F78BF">Actualizar datos</h4>
                        <br>
                        
                        <label>Descripción:</label>
                        <input type="text" required="" name="descripcionU"  id="descripcionU" class="form-control">
                        <label>Dinero utilizado:</label>
                        <input type="number" required="" name="montoUtilizadoU" id="montoUtilizadoU" class="form-control" >
                        <label>Detalle de boleta:</label>
                        <input type="text" required="" name="detalleBoletaU" id="detalleBoletaU" class="form-control" >
                        <label>Fecha:</label>
                        <input type="date" required="" name="fechaU" id="fechaU" class="form-control">
                        
                        <label>archivo:</label>
                        <input type="file" name="archivoE" id="archivoE" class="form-control" onchange="return archivo()" />
                        <label>Nombre empresa:</label>
                        <input type="text" required="" name="nombreEE" placeholder="Nombre Empresa"  id="nombreEE" class="form-control"/>
                        <label>Rut empresa:</label>
                        <input type="text" name="rutEE" id="rutEE" placeholder="rut Empresa" class="form-control" onchange="validaRut(this)" required="">
                        
                        <input type="hidden" id="idA" value="{{ $id }}" name="idA">
                        <input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">
                        
                        <br>
                        <input type="submit" value="Editar" href="" class="btn btn-info form-control btnEditar"> </input>
                        <br>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/gastos.js') }}"></script>
@endsection