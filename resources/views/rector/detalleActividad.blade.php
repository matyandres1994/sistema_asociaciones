@extends('layouts.rector')
@section('content')
<h2 class="panel-body text-center" style="color:#333390">Actividades</h2>

<div class="actividades">
    <div  class="tablaA"  id="{{$id}}" url="{{ env('APP_URL') }}">
        
    </div>
</div>
<!-- modal ingresar -->
<div id="modalingresar" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content " style="filter:alpha(opacity=50); opacity:0.9;">
            <div class="container">
                <form id="formIngresar" action="{{ route('crearActividad') }}" method="POST">
                    <h4 style="text-align: center; color: #3F78BF">Insertar datos</h4>
                    <br>
                    <div id="alerta1" class="alert alert-warning" role="alert"></div>
                    <label>Nombre:</label>
                    <input type="text" required="" name="nombreA"  id="nombreA" class="form-control">
                    <label>Objetivo:</label>
                    <input type="text" required="" name="objetivoA" id="objetivoA" class="form-control" >
                    <label>Fecha:</label>
                    <input type="date" required="" name="fechaA" min="2019-07-24" id="fechaA" class="form-control" >
                    <label>Hora:</label>
                    <input type="time" required="" name="horaA" id="horaA" class="form-control">
                    <label>Lugar:</label>
                    <input type="text" required="" name="lugarA" id="lugarA" class="form-control">
                    <label>Descripcion:</label>
                    <input type="text" required="" name="descripcionA" id="descripcionA" class="form-control">
                    
                    <input type="hidden" id="idA" value="{{ $id }}" name="idA">
                    <input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">
                    
                    <br>
                    <a  class="btn btn-info form-control ingresar" id="ingresar"> ingresar</a>
                    <br>
                    
                </form>
                
            </div>
        </div>
    </div>
</div>
<!-- modal editar -->
<div id="modaleditar" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content " style="filter:alpha(opacity=50); opacity:0.9;">
            <div class="container">
                <form action="  " method="POST">
                    <br>
                    <h4 style="text-align: center; color: #3F78BF">Actualizar datos</h4>
                    <br>
                    
                    <div id="alerta2" class="alert alert-warning" role="alert"></div>
                    <label>Nombre:</label>
                    <input type="text" required="" name="nombreAE"  id="nombreAE" class="form-control">
                    <label>Objetivo:</label>
                    <input type="text" required="" name="objetivoAE" id="objetivoAE" class="form-control" >
                    <label>Fecha:</label>
                    <input type="date" required="" name="fechaAE" id="fechaAE" class="form-control" >
                    <label>Hora:</label>
                    <input type="text" required="" name="horaAE" id="horaAE" class="form-control">
                    <label>Lugar:</label>
                    <input type="text" required="" name="lugarAE" id="lugarAE" class="form-control">
                    <label>Descripcion:</label>
                    <input type="text" required="" name="descripcionAE" id="descripcionAE" class="form-control">
                    
                    
                    
                    <input type="hidden" id="idA" value="{{ $id }}" name="idA">
                    <input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">
                    
                    <br>
                    <a href="" class="btn btn-info form-control btnEditar"> Editar</a>
                    <br>
                    
                </form>
                
            </div>
        </div>
    </div>
</div>

</div>



@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/actividades.js') }}"></script>
@endsection