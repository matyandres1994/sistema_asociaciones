<table class="table table-hover">
    <thead>
        <tr>
            <th class="text-center">Nombre</th>
            <th class="text-center">Descripcion</th>
            <th class="text-center">Detalles</th>
            <th class="text-center">Integrantes</th>
            <th class="text-center"></th>
            <th class="text-center"><!-- Button modal agregar -->
          <!--  <button id="botonagregar" type="button" class="btn btn-primary">
        +
        </button>-->
        </i></a></th>
    </tr>
</thead>
<tbody>
    
    @foreach ($asociaciones as $element)
    <tr id="{{$element->id}}">
        <th class="text-center" class="text-center">{{ $element->nombre }}</th>
        <th class="text-center">{{ $element->descripcion }}</th>
        <th class="text-center">{{ $element->objetivo }}</th>
        
        
        <th class="text-center">
            
            <form action="{{ route('miembrosasociaciones',$element->id) }}" type="POST">
                <input class="btn btn-info" type="submit" value="Integrantes">
            </form>
        </th>
        <!--<th class="text-center">
        <button id="botoneditar" type="button" class="botoneditar btn btn-primary"  data-toggle="modal" data-target="#modaleditar">
        Editar
<th></button>
        <td><a class="eliminaras btn btn-primary" id="eliminaras" >Eliminar</a></td></th>
-->
    </i></a></th>
    
</tr>
@endforeach

</tbody>
</table>