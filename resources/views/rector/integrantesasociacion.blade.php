@extends('layouts.rector')
@section('meta')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
<h2 class="text-center" style="color: #110F63">Miembros de la agrupación</h2>
<br><br>
<table class="table table-hover">
	<thead>
		<tr>
			<th class="text-center">Nombre</th>
			<th class="text-center">Email</th>
			<th class="text-center">Cargo</th>
			
			
		</tr>
	</thead>
	<tbody>
		
		@foreach ($Miembros as $element)
		<tr>
			<th class="text-center" class="text-center">{{ $element->name }}</th>
			<th class="text-center">{{ $element->email }}</th>
			<th class="text-center">{{ $element->nombre }}</th>
			
			
		</tr>
		@endforeach
		
	</tbody>
</table>

@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/gestionasociacion.js') }}"></script>
@endsection