@extends('layouts.dde')
@section('meta')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
<div id="tablaF" url="{{ env('APP_URL') }}">
</div>
<!-- modal ingresar -->
<div id="modalVer" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content " style="filter:alpha(opacity=50); opacity:0.9;">
            <div class="container">
            <form id="formIngresar" action="" method="POST">
                {{ csrf_field() }}
                <br>
                <h4 style="text-align: center; color: #3F78BF">Insertar datos</h4>
                <br/>
                
                    <label>Fecha</label>
                    
                    
                    
                    <div class="row">
                    <select name="select" id="fecha" class="form-control col-6">
                        
                        <option value="2018">2018</option>
                        <option value="2019" selected>2019</option>
                        <option value="2020" >2020</option>
                        <option value="2021">2021</option>
                        <option value="2022" >2022</option>
                    </select>
                    
                    
                    
                    <br/>
                    <button   class="btn btn-info form-control ingresar offset-1 col-5" id="ingresar"> Buscar  <i class="fas fa-search"></i></button>
                    <br>
                    </div>
                
                <br><br>
                <hr>
                <div id="datos">
                    <table class="table table-inverse">
                        <thead>
                            <tr>
                                <th class="text-center">Nombre</th>
                                <th class="text-center">Descripcion</th>
                                
                                
                            </tr>
                        </thead>
                        <tbody>
                            
                            
                        </tbody>
                    </table>
                    
                </div>
                <br><br><br>
            </form>
            </div>
        </div>
    </div>
</div>
</div>
<!-- modal editar -->
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/InforFinanciera.js') }}"></script>
@endsection