

@extends('layouts.dde')

@section('content')
<div class="finanza">
                        @isset ($Gastos)
                            <h2 class="panel-body text-center" style="color:#333390">Rendir gastos </h2>
                        @endisset
                        @isset ($asociaciones)
                            <h2 class="panel-body text-center" style="color:#333390">Mis agrupaciones </h2>
                        @endisset
                        
                        <br>
                        @isset ($asociaciones)
                            <table class="table table-inverse">
                            <thead>
                                <tr>
                                    <th class="text-center">Nombre</th>
                                    <th class="text-center">Descripcion</th>
                                    <th class="text-center">Detalles</th>

                                </tr>
                            </thead>
                            <tbody>
                                
                                    @foreach ($asociaciones as $element)
                                    <tr>
                                             <th class="text-center" class="text-center">{{ $element->nombre }}</th>
                                             <th class="text-center">{{ $element->descripcion }}</th>
                                             <th class="text-center">
                                                
                                                <form action="{{ route('detallefinanza',$element->id) }}" type="POST">
                                                    <input class="btn btn-info" type="submit" value="Ver detalle">
                                                </form>
                                             </th>
                                    </tr>
                                    @endforeach
                                
                            </tbody>
                        </table>
                        @endisset
</div>

<div class="gastos">
                           @isset ($Gastos)
                            
                            <table class="table table-inverse">
                                <thead>
                                    <tr>
                                        <th class="text-center">Descripcion</th>
                                        <th class="text-center">Monto Utlizado</th>
                                        <th class="text-center">Detalle boleta</th>
                                        <th class="text-center">Archivo</th>

                                       <th class="text-center"><a href=""><i class="fas fa-plus"></i></a></th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($Gastos as $element)
                                    <tr>
                                        <th class="text-center" class="text-center">{{ $element->descripcion }}</th>
                                        <th class="text-center">{{ $element->montoUtilizado}}</th>
                                        <th class="text-center" class="text-center">{{ $element->detalleBoleta }}</th>
                                        <th class="text-center">{{ $element->archivo}}</th>
                                         <th class="text-center"><a href="{{  route('detallefinanza', $element->id)  }}"> <i class="fas fa-trash-alt"></i></a>
                                        <a href="{{  route('detallefinanza', $element->id)  }}"><i class="fas fa-align-justify"></a></th>
                                    </tr>
                                    @endforeach
                                    
                                </tbody>
                            </table>
                        @endisset  
</div>
                       
                            
                       
                        
                       
      

@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/finanza.js') }}"></script>
@endsection