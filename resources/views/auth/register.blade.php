<!DOCTYPE html>
<html>
<head>

    <title>Registro</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="//ubiobio.cl/favicon.ico" type="image/x-icon">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->

    <link rel="stylesheet" type="text/css" href="{{ asset('css/login1.css') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style type="text/css">
        .matytxt{
            border-radius: 5px;
            border: 1px solid #39c;
        }
        .matytxt:focus{
            outline: none;
            outline-width: none;
            box-shadow: none;
        }
        body{
            background: url(img/auth/AuthFondo.jpg) no-repeat center center fixed;
        
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            
        }
        

    </style>
</head>



<body>


<div class="container" style="margin-top: 12%;">
<h2 class="panel-body text-center" style="color:#FFFFFF">Registro Cuentas</h2>

    <div class="row">
    


         <label style="font-size: 20px;color:#FCFCFF;bottom: -10px; text-align: center;" class="col-6 offset-3">Asociaciones Estudiantiles</label>
<hr>


        <div class="col-6 offset-3">

            <div class="card  bg-light" >
            <div class="panel-heading"></div>
            <br>

<div class="panel-body">
    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="col-md-4 control-label">Nombre</label>

            <div class="col-md-6">
                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="col-md-4 control-label">Correo</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password" class="col-md-4 control-label">Contraseña</label>

            <div class="col-md-6">
                <input id="password" type="password" class="form-control" name="password" required>

                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>

      

        <div class="form-group">
            <label for="password-confirm" class="col-md-4 control-label">Repetir Contraseña</label>

            <div class="col-md-6">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
            </div>
        </div>

          <div class="form-group{{ $errors->has('rol') ? ' has-error' : '' }}">
            <label for="name" class="col-md-4 control-label">Rol</label>

            <div class="col-md-6">
                <select id="rol" class="form-control" name="rol" value="{{ old('rol') }}" required autofocus>
                    <option value="administrador">Administrador</option>
                    <option value="rector">Rector</option>
                    <option value="alumno">Alumno</option>
                    <option value="dde">DDE</option>
                </select> 

                @if ($errors->has('rol'))
                    <span class="help-block">
                        <strong>{{ $errors->first('rol') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <br>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    Registrar
                </button>
            </div>
        </div>
                    </form>
                </div>
            </div>
        </div>
        <label style="font-size: 15px;color:#FCFCFF;bottom: 4px; text-align: center;" class="col-6 offset-3">Universidad del bio bio</label>
    </div>
</div>

</div>
</body>
</html>




