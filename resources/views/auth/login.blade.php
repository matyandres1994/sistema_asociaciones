<!DOCTYPE html>
<html>
<head>
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="//ubiobio.cl/favicon.ico" type="image/x-icon">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/login1.css') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style type="text/css">
        .matytxt{
            border-radius: 5px;
            border: 1px solid #39c;
        }
        .matytxt:focus{
            outline: none;
            outline-width: none;
            box-shadow: none;
        }
        body{
            background: url(img/auth/AuthFondo.jpg) no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

    </style>
</head>
<body >
    <div class="container" style="margin-top: 12%;">


    <div class="row">


         <label style="font-size: 20px;color:#FCFCFF;bottom: -10px; text-align: center;" class="col-6 offset-3">Asociaciones Estudiantiles</label>




        <div class="col-6 offset-3">
            <div class="card  bg-light" >
                <div class="card-header" >Login</div>

                <div class="card-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="row form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-3 control-label">Email:</label>

                            <div class="col-9">
                                <input id="email"  type="email" class="form-control matytxt" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-3 control-label" style="font-size: 15px">Password:</label>

                            <div class="col-9">
                                <input id="password" type="password" class="form-control matytxt" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group row">
                            <div class="col-12">
                                <button type="submit" class="btn btn-outline-dark col-12">
                                    Ingresar
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <label style="font-size: 15px;color:#FCFCFF;bottom: 4px; text-align: center;" class="col-6 offset-3">Universidad del bio bio</label>
    </div>
</div>
</body>
</html>
