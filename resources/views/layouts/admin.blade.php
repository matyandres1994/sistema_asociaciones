<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" id="bootstrap-css" rel="stylesheet">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js">
</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
</script>
<!------ Include the above in your HEAD tag ---------->
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <meta content="Responsive sidebar template with sliding effect and dropdown menu based on bootstrap 3" name="description">
        @yield('meta')
        <title>
        {{ config('app.name', 'Laravel') }}
        </title>
        <link href="//ubiobio.cl/favicon.ico" rel="shortcut icon" type="image/x-icon">
        <link crossorigin="anonymous" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" rel="stylesheet">
        <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
        <link href="{{ asset('css/sidebar.css') }}" rel="stylesheet" type="text/css">
        <script src="{{ asset('js/sidebar.js') }}" type="text/javascript">
        </script>
        <link href="{{ asset('toast/toastr.min.css')}}" rel="stylesheet" type="text/css">
    </link>
    <style type="text/css">
        .loader {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('{{ asset('img/loading.gif') }}') 50% 50% no-repeat rgb(249,249,249);
    opacity: .6;
}
    </style>
</link>
</link>
</link>
</link>
</meta>
</meta>
</meta>
</meta>
</head>
<body>
<div class="page-wrapper chiller-theme toggled">
<a class="btn btn-sm btn-dark" href="#" id="show-sidebar">
<i class="fas fa-bars">
</i>
</a>
<nav class="sidebar-wrapper" id="sidebar">
<div class="sidebar-content">
<div class="sidebar-brand">
    <input class="align-middle" disabled="" src="{{ asset('img/logoubb.png')}}" style="margin-right: 10px;" type="image" width="25">
    <a href="{{  route('home') }} ">
        {{ config('app.name', 'Laravel') }}
    </a>
    <div id="close-sidebar">
        <i class="fas fa-times">
        </i>
    </div>
    </input>
</div>
<div class="sidebar-header">
    <div class="user-info">
        <span class="user-name">
            {{ Auth::user()->name }}
        </span>
        <span class="user-role">
            {{Auth::user()->rol}}
        </span>
        <span class="user-status">
            <i class="fa fa-circle">
            </i>
            <span>
                Online
            </span>
        </span>
    </div>
</div>
<!-- sidebar-search  -->
<div class="sidebar-menu">
    <ul>
        <li class="header-menu">
            <span>
                General
            </span>
        </li>
        <li class="sidebar-dropdown">
            <a href="#">
                <i class="fa fa-user-circle">
                </i>
                <span>
                    Mi asociaciones
                </span>
            </a>
            <div class="sidebar-submenu">
                <ul>
                   
                    <li>
                        <a href="{{ route('actividades') }}">
                            Actividades
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('Finanzas.index') }}">
                            Info financiera
                        </a>
                    </li>
                   
                </ul>
            </div>
        </li>
        <li class="sidebar-dropdown">
            <a href="#">
                <i class="fa fa-exclamation-triangle">
                </i>
                <span>
                    Denuncias
                </span>
            </a>
            <div class="sidebar-submenu">
                <ul>
                    <li>
                        <a href="{{ route('denuncias') }}">
                            Ver denuncias
                        </a>
                    </li>
                   
                </ul>
            </div>
        </li>
        <li class="sidebar-dropdown">
            <a href="#">
                <i class="fa fa-users">
                </i>
                <span>
                    Asociaciones
                </span>
            </a>
            <div class="sidebar-submenu">
                <ul>
                    <li>
                        <a href="{{ route('asociaciones') }}">
                            Ver asociaciones
                        </a>
                    </li>
                    
                </ul>
            </div>
        </li>
        <li class="sidebar-dropdown">
            <a href="#">
                <i class="fa fa-exclamation-circle">
                </i>
                <span>
                    Solicitudes
                </span>
            </a>
            <div class="sidebar-submenu">
                <ul>
                    <li>
                        <a href="{{ route('solicitudes.index') }}">
                            Ver solucitud
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="sidebar-dropdown">
            <a href="#">
                <i class="far fa-money-bill-alt">
                </i>
                <span>
                    Informacion financiera
                </span>
            </a>
            <div class="sidebar-submenu">
                <ul>
                    <li>
                        <a href="{{ route('verGastos') }}">
                            Ver informacion
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="sidebar-dropdown">
            <a href="#">
                <i class="fa fa-book">
                </i>
                <span>
                    Actas
                </span>
            </a>
            <div class="sidebar-submenu">
                <ul>
                    
                    <li>
                        <a href="{{ route('actas.index') }}">
                            Ver actas
                        </a>
                    </li>
                    
                </ul>
            </div>
        </li>
        <li class="sidebar-dropdown">
            <a href="#">
                <i class="fab fa-github-alt">
                </i>
                <span>
                    Cuentas
                </span>
            </a>
            <div class="sidebar-submenu">
                <ul>
                    <li>
                        <a href="{{ route('vercuentas') }}">
                            Ver cuentas
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('register') }}">
                            Agregar cuentas
                        </a>
                    </li>
                    
                </ul>
            </div>
        </li>
    </ul>
</div>
<!-- sidebar-menu  -->
</div>
<!-- sidebar-content  -->
<div class="sidebar-footer">
<a href="#">
    <i class="fa fa-bell">
    </i>
</a>
<a href="#">
    <i class="fa fa-envelope">
    </i>
</a>
<a href="#">
    <i class="fa fa-cog">
    </i>
</a>
<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
    <span class="badge-sonar">
    </span>
    <i class="fa fa-power-off">
    </i>
</a>
<form action="{{ route('logout') }}" id="logout-form" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>
</div>
</nav>
<!-- sidebar-wrapper  -->
<main class="page-content">
<div class="container-fuild">
<div class="container-fluid" style="background: white;width: 100%;  height: 100%">
    @yield('content')
</div>
</div>
</main>
</div>
</body>
</html>
</link>
<!-- page-content" -->
<!-- page-wrapper -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js">
</script>
<script src="{{ asset('toast/toastr.min.js') }}" type="text/javascript">
</script>
<script crossorigin="anonymous" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js">
</script>
<script crossorigin="anonymous" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js">
</script>

@yield('script')