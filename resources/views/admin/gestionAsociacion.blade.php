

@extends('layouts.admin')
@section('meta')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection





<script>
  function sololetras(e){
  key=e.keyCode || e.which;
  teclado=String.fromCharCode(key).toLowerCase();
  letras="abcdefghijklmnñopqrstuvwxyz";
  especiales="8-37-38-46-164";
  teclado_especial=false;
  for(var i in especiales){
  if(key==especiales[i]){
  teclado_especial=true;break;
    }
    }
    if(letras.indexOf(teclado)==-1 && !teclado_especial){
    return false;

  }
  }
</script>


@section('content')
<div class="ver asociaciones">
  
  
  <h2 class="panel-body text-center" style="color:#333390">Asociaciones</h2>
  
  
  <br>
  <div id="alerta" class="alert alert-success" role="alert"></div>
  <div url="{{ route('eliminarasociacion') }}" id="tabla" class="tabla"></div>


  
  
</div>

<!-- Modal Editar -->
<div id="modaleditar" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="titulo">Editar Asociación</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id='formeditar' url="{{env('APP_URL')}}" >
          {{ csrf_field() }}
          <div id="alerta2" class="alert alert-warning" role="alert"></div>
          <label>Nombre:</label>
          <input type="text" name="nombre"  id="nombre1" class="form-control" onkeypress="return sololetras(event)">
          <br>
          <label>Descripción:</label>
          <input type="text" name="descripcion" id="descripcion1" class="form-control"">
          <br>
          <label>Detalles:</label>
          <input type="text" name="detalles" id="detalles1" class="form-control" onkeypress="return sololetras(event)">
          <input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">
        </form>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button id="btnedit" class="btn btn-primary">Editar</button>
      </div>
    </div>
  </div>
</div>

<!-- modal ingresar -->




<div id="modalingresar" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content " style="filter:alpha(opacity=50); opacity:0.9;">
      <div class="container">
        <form id="formIngresar" action="{{ route('agregarasociacion') }}" method="POST">
          <br>
          <h4 style="text-align: center; color: #3F78BF">Insertar datos</h4>
          <br>
          <div id="alerta1" class="alert alert-warning" role="alert"></div>
          <label>Nombre:</label>
          <input type="text" required="" name="nombre"  id="nombre2" class="form-control">
          <label>Descripción:</label>
          <input type="" required="" name="descripcion" id="descripcion2" class="form-control" >
          <label>Correo:</label>
          <input type="email" required="" name="correo" id="correo2" class="form-control" placeholder="usuario@gmail.com" >
          <label>Objetivo:</label>
          <input type="text" required="" name="objetivo" id="objetivo2" class="form-control">
          <br>



          <label>Directiva</label><hr>
          
 <label for="sel1">Presidente:</label>
      <select class="form-control" id="presi">
        <option value="Elegir">Elegir</option>
        @foreach ($email as $element)
          <option value="{{$element->id}}">{{$element->email}}</option>
        @endforeach
      </select>
  <br>
<br>
               
  <label for="sel1">Tesorero:</label>
    
       <select class="form-control" id="teso">
        <option value="Elegir">Elegir</option>
        @foreach ($email as $element)
          <option value="{{$element->id}}">{{$element->email}}</option>
        @endforeach
      </select>
   
  <br>

         <br> 
            
  <label for="sel1">Secretario:</label>
     
         <select class="form-control" id="secre">
          <option value="Elegir">Elegir</option>
        @foreach ($email as $element)
          <option value="{{$element->id}}">{{$element->email}}</option>
        @endforeach
      </select>
      
  <br>
<br>
          
          
          <input type="hidden" id="idA" value="" name="idA">
          <input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">
          
          <br>
          <a  class="btn btn-info form-control ingresar" id="ingresar"> Ingresar</a>
          <br>
          <br>
          
        </form>
        
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/asociaciones.js') }}"></script>
@endsection