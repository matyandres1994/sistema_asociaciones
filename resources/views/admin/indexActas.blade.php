@extends('layouts.admin')
@section('meta')
<meta content="{{ csrf_token() }}" name="csrf-token">
@endsection
@section('content')
<div class="actas">
	<h2 class="panel-body text-center" style="color:#333390">
	Solicitudes de asociaciones
	</h2>
	<br>
	</br>
	<div class="loader" ></div>
	<div id="tablaSolicitudes" url="{{ env('APP_URL') }}">
		
	</div>
	<div id="modaleditar" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content " style="filter:alpha(opacity=50); opacity:0.9;">
				<div class="container">
					<div id="info"></div>
					
				</div>
			</div>
		</div>
	</div>
	<div id="modalFormulario" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content " style="filter:alpha(opacity=50); opacity:0.9;">
				<div class="container">
					<form id="formularioActa" action="{{ route('solicitudes.store') }}">
						{{ csrf_field() }}  
						<br><br>
						<h4 class="text-center">Formulario envio acta</h4>
						<label>Descripcion:</label>
						<input type="text" class="form-control" id="descripcion" placeholder="Descripcion" 	name="descripcion">
						<label>Comentario:</label>
						<input type="text" class="form-control" id="comentario" placeholder="Comentario" 	name="comentario">
						<label>Archivo:</label>
						<input type="file" class="form-control" id="archivo" onchange="return fileValidation()"	name="archivo">
						<input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}" />
						<br>
						<button type="button" id="btnEA" class="btn btn-outline-success form-control">Enviar</button>
					</form>
					
				</div>
			</div>
		</div>
	</div>
	
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/actas.js') }}"></script>
@endsection