@extends('layouts.admin')
@section('content')
<h2 class="panel-body text-center">
Bienvenido {{auth::user()->name}}
</h2>
<p class="text-center">
	Has logeado como {{ auth::user()->rol }}
</p>
@endsection