<table class="table table-inverse">
    <thead>
        <tr>
            <th class="text-center">
                Descripcion
            </th>
            <th class="text-center">
                Monto Utlizado
            </th>
            <th class="text-center">
                Detalle boleta
            </th>
            <th class="text-center">
                Fecha
            </th>
            <th class="text-center">
                Archivo
            </th>
            <th class="text-center">
                Nombre E.
            </th>
            <th class="text-center">
                Rut E.
            </th>
            <th class="text-center">
                <a href="" id="btnIngresar">
                    <i class="fas fa-plus">
                    </i>
                </a>
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($Gastos as $element)
        <tr id="{{ $element->id }}">
            <th class="text-center">{{$element->descripcion}}</th>
            <th class="text-center">{{ $element->montoUtilizado}}</th>
            <th class="text-center">{{ $element->detalleBoleta }}</th>
            <th class="text-center">{{ $element->fecha}}</th>
            <th class="text-center">
                <a download="{{ $element->archivo }}" href="/download/{{$element->archivo}}">
                    {{ $element->archivo }}
                </a>
            </th>
            <th class="text-center">{{ $element->nombreE}}</th>
            <th class="text-center">{{ $element->rutE}}</th>
            <th class="text-center">
                <a class="btnEliminar" href="" id="Eliminarbtn" url="{{ env('APP_URL') }}:8000/eliminar/">
                    <i class="fas fa-trash-alt">
                    </i>
                </a>
                <a class="btnEdit" href="" id="Editarbtn" url="{{ env('APP_URL') }}:8000/editar/">
                    <i class="fas fa-align-justify">
                    </i>
                </a>
            </th>
        </tr>
        @endforeach
    </tbody>
</table>