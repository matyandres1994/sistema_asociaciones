@extends('layouts.admin')
@section('meta')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')

<div id="divURL" url="{{env('APP_URL')}}">

	<h2 class="panel-body text-center" style="color:#333390">Cuentas </h2>
	<br>
	<br>
	

<table class="table table-inverse">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Correo</th>
            <th>Rol</th>
			
           
        </tr>
    </thead>
    <tbody>
@foreach ($Asociaciones as $cuentas)
    <tr">
            <td>{{$cuentas->name }}</td>
            <td>{{$cuentas->email }}</td>
            <td>{{$cuentas->rol }}</td>
            <td><a href="{{ route('eliminarCuenta',$cuentas->id) }}" class="btn btn-info btnEliminar" id="eliminarcuenta" >Eliminar</a></td>

        </tr>
@endforeach

        
    </tbody>
</table>






</div>

@endsection

@section('script')
<script type="text/javascript" src="{{ asset('js/cuentas.js') }}"></script>
@endsection
