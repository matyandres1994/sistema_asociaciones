<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Document</title>
		<link crossorigin="anonymous" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" rel="stylesheet">
	</head>
	<body>
		<div>
			<h1 class="text-center" >Solicitud para crear una nueva asociacion</h1>
			<br>
			<p>Se solicita a travez del presente aprobación para crear asociacion con la siguiente informacion.
			</p>
			@php
			$count = 0;
			@endphp
			<br><br>
			<h4 class="text-center">Detallles de la agrupacion</h4>
			<br>
			<table class="table table-hover">
				<thead>
					<tr>
						<th class="text-center">Nombre</th>
						<th class="text-center">Cargo</th>
						<th class="text-center">Email</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($integrantes as $element)
					@php
					$count++;
					@endphp
					
					<tr>
						<td class="text-center">{{ $element->name }}</td>
						<td class="text-center">{{ $element->nombre }}</td>
						<td class="text-center">{{ $element->email }}</td>
					</tr>
					
					@endforeach
					
				</tbody>
			</table>
			
			<table class="table table-hover">
				<thead>
					<tr>
						<th class="text-center">Asociacion</th>
						<th class="text-center">Objetivo</th>
						<th class="text-center">Numero de integrantes</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($asociacion as $element)
					
					<tr>
						<td class="text-center">{{ $element->nombre }}</td>
						<td class="text-center">{{ $element->objetivo }}</td>
						<td class="text-center">@php
							echo $count;
						@endphp</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			
			<br><br><br><br>
			<div class="row">
				<p class="offset-1 col-3">_____________</p>
				<p class="offset-8 col-3">_____________</p>

			</div>
			<div class="row">
				<p class="offset-1 col-3">Firma Decanatura</p>
				<p class="offset-8 col-3">Timbre</p>
				
			</div>
		</div>
	</body>
</html>