<table class="table table-inverse">
    <thead>
        <tr>
            <th class="text-center">Detalle</th>
            <th class="text-center">fecha</th>           
            <th class="text-center">hora</th>
            <th class="text-center">lugar</th>
     

           <th class="text-center"><a id="btnIngresar" href=""><i class="fas fa-plus"></i></a></th>
                                        
        </tr>
    </thead>
    <tbody>
        
        @foreach ($Actividades as $element)
        <tr id={{ $element->id }}>
            <th class="text-center" class="text-center">{{ $element->detalleD}}</th>
            <th class="text-center">{{ $element->fechaD}}</th>
            <th class="text-center" class="text-center">{{ $element->horaD}}</th>
            <th class="text-center">{{ $element->ludarD}}</th>
    

            <th class="text-center"><a href="" class="btnEliminar" > <i class="fas fa-trash-alt"></i></a>
            <a href="" class="btnEdit" ><i class="fas fa-align-justify"></i></a></th>
        </tr>
        @endforeach
    </tbody>
</table>
