<table class="table table-inverse">
    <thead>
        <tr>
            <th class="text-center">Nombre</th>
            <th class="text-center">Descripcion</th>
            
            
        </tr>
    </thead>
    <tbody>
        
        @foreach ($asociaciones as $elementos)
        <tr id="{{ $elementos->id }}">
            <th class="text-center" class="text-center">{{ $elementos->nombre }}</th>
            <th class="text-center">{{ $elementos->descripcion}}</th>
            
            <th class="text-center"><a href="" class="btnVer" > <i class="fas fa-search"></i></a>
        </th>
    </tr>
    @endforeach
</tbody>
</table>