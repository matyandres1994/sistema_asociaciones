<div>
	@php
	$count = 0;
	@endphp
	<br>
	<h4 class="text-center">Detallles de la agrupacion</h4>
	<br>
	<table class="table table-hover">
		<thead>
			<tr>
				<th class="text-center">Nombre</th>
				<th class="text-center">Cargo</th>
				<th class="text-center">Email</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($integrantes as $element)
			@php
			$count++;
			@endphp
			
			<tr>
				<td class="text-center">{{ $element->name }}</td>
				<td class="text-center">{{ $element->nombre }}</td>
				<td class="text-center">{{ $element->email }}</td>
			</tr>
			
			@endforeach
			
		</tbody>
	</table>
	
	<table class="table table-hover">
		<thead>
			<tr>
				<th class="text-center">Asociacion</th>
				<th class="text-center">Objetivo</th>
				<th class="text-center">Numero de integrantes</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($asociacion as $element)
			
			<tr>
				<td class="text-center">{{ $element->nombre }}</td>
				<td class="text-center">{{ $element->objetivo }}</td>
				<td class="text-center">@php
					echo $count;
				@endphp</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	<a id="btnGenerar" href="{{ route('pdf',$id)}}" class="form-control btn btn-outline-success">Generar y enviar acta</a>
	
	
	<br><br>
</div>