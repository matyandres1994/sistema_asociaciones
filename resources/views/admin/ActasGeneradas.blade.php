@extends('layouts.admin')
@section('meta')
<meta content="{{ csrf_token() }}" name="csrf-token">
@endsection
@section('content')

<h4 class="text-center">Actas generadas</h4>
<div class="loader"></div>
<div id="tablasActas" urlNativo="{{ env('APP_URL') }}"></div>
<div id="modalACTA" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content " style="filter:alpha(opacity=50); opacity:0.9;">
				<div class="container">
					<form id="formularioActa" action="{{ route('actualizar') }}">
						{{ csrf_field() }}  
						<br><br>
						<h4 class="text-center">Formulario envio acta</h4>
						
						<input type="radio"  name="opcion" id="opcion" value="1">Aceptar <br>
						<input type="radio"  name="opcion" id="" value="2">Rechazar
						<br><br>
						<label>Archivo:</label>
						<input type="file" class="form-control" id="archivo" onchange="return fileValidation()"	name="archivo">
						<input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}" />
						<br>
						<button type="button" id="btnEA" class="btn btn-outline-success form-control">Enviar</button>
					</form>
					
				</div>
			</div>
		</div>
	</div>
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/aGeneradas.js') }}"></script>
@endsection