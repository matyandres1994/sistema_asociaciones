@extends('layouts.admin')
@section('meta')
<meta content="{{ csrf_token() }}" name="csrf-token">
    @endsection
@section('content')
    <div class="finanza">
        @isset ($asociaciones)
        <h2 class="panel-body text-center" style="color:#333390">
            Mis asociaciones
        </h2>
        @endisset
        <br>
            @isset ($asociaciones)
            <table class="table table-inverse">
                <thead>
                    <tr>
                        <th class="text-center">
                            Nombre
                        </th>
                        <th class="text-center">
                            Descripcion
                        </th>
                        <th class="text-center">
                            Detalles
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($asociaciones as $element)
                    <tr>
                        <th class="text-center">
                            {{ $element->nombre }}
                        </th>
                        <th class="text-center">
                            {{ $element->descripcion }}
                        </th>
                        <th class="text-center">
                            <form action="{{ route('detallefinanza',$element->id) }}" type="POST">
                                <input class="btn btn-info" type="submit" value="Info financiera">
                                </input>
                            </form>
                        </th>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @endisset
        </br>
    </div>
    @endsection
</meta>