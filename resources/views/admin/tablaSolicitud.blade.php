<table class="table table-inverse">
	<thead>
		<tr>
			<th class="text-center">Nombre</th>
			<th class="text-center">Descripcion</th>
		</tr>
	</thead>
	@foreach ($asociaciones as $element)
	<tbody>
		<tr id="{{ $element->id }}">
			<td class="text-center">{{ $element->nombre }}</td>
			<td class="text-center">{{ $element->descripcion }}</td>
			<td class="text-center"><a href="" id="btnDetalles" class="btnDetalles btn btn-outline-info">Ver detalles</a></td>
		</tr>
	</tbody>
	@endforeach
</table>