<br><table class="table table-hover">
	<thead>
		<tr>
			<th class="text-center">Descripcion</th>
			<th class="text-center">Comentario</th>
			<th class="text-center">Archivo</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($actas as $element)
		
		<tr id="{{ $element->id }}" idA="{{ $element->idA }}">
			<td class="text-center">{{ $element->descripcion }}</td>
			<td class="text-center">{{ $element->comentario }}</td>
			<th class="text-center">
                <a download="{{ $element->archivo }}" href="/downloadActa/{{$element->archivo}}">
                    {{ $element->archivo }}
                </a>
            </th>
			<th class="text-center"><a class="btnIngresar" href=""><i class="fas fa-align-justify"></a></th>
		</tr>
		@endforeach
	</tbody>
</table>