<table class="table table-inverse">
    <thead>
        <tr>
            <th class="text-center">Nombre</th>
            <th class="text-center">Descripción</th>           
            <th class="text-center">Correo</th>
            <th class="text-center">Objetivo</th>
            <th class="text-center">Estado</th>

           <th class="text-center"><a id="btnIngresar" href=""><i class="fas fa-plus"></i></a></th>
                                        
        </tr>
    </thead>
    <tbody>
        
        @foreach ($Asociaciones as $element)
        <tr>
            <th class="text-center" class="text-center">{{ $element->nombre }}</th>
            <th class="text-center">{{ $element->descripcion}}</th>
            <th class="text-center" class="text-center">{{ $element->correo }}</th>
            <th class="text-center">{{ $element->objetivo}}</th>
            <th class="text-center">{{ $element->estado}}</th>
            <th class="text-center"><a href="" class="btnEliminar" id="{{ $element->id }}"> <i class="fas fa-trash-alt"></i></a>
            <a href="" class="btnEdit" id="{{ $element->id }}"><i class="fas fa-align-justify"></i></a></th>
        </tr>
        @endforeach
    </tbody>
</table>
