@extends('layouts.estudiantes')
@section('content')
<h2 class="panel-body text-center" style="color:#333390">
Denuncias
</h2>
<div class="denuncias">
    <div class="tablad" id="{{$id}}" url="{{ env('APP_URL') }}">
    </div>
</div>
<!-- modal ingresar -->
<div id="modalingresar" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content " style="filter:alpha(opacity=50); opacity:0.9;">
            <div class="container">
                <form id="formIngresar" action="{{ route('crearDenuncia') }}" method="POST">
                    <h4 style="text-align: center; color: #3F78BF">Insertar datos</h4>
                    <br>
                    <div id="alerta1" class="alert alert-warning" role="alert"></div>

                    <label>Detalle:</label>
                    <input type="text" required="" name="detalleD"  id="detalleD" class="form-control">
                    
                    <label>Lugar:</label>
                    <input type="text" required="" name="ludarD" id="ludarD" class="form-control" >
                    
                    <input type="hidden" required="" value="{{ date('Y-m-d')}}" name="fechaD" id="fechaD" class="form-control" >
                    <input type="hidden" required="" value="{{ date('H:i') }}" name="horaD" id="horaD" class="form-control">
                    <input type="hidden" id="idA" value="{{ $id }}" name="idA">
                    <input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">
                    
                    <br>
                    <a  class="btn btn-info form-control ingresar" id="ingresar"> ingresar</a>
                    <br>
                    
                </form>
                
            </div>
        </div>
    </div>
</div>
<!-- modal editar -->
<div id="modaleditar" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content " style="filter:alpha(opacity=50); opacity:0.9;">
            <div class="container">
                <form action="  " method="POST">
                    <br>
                   <br>
                    
                    <label>Detalle:</label>
                    <input type="text" required="" name="detalle"  id="detalle" class="form-control">
                    <label>Fecha:</label>
                    <input type="hidden" required="" name="fecha" id="fecha" class="form-control" >
                    <label>Hora:</label>
                    <input type="hidden" required="" name="hora" id="hora" class="form-control" >
                    <label>Lugar:</label>
                    <input type="text" required="" name="ludar" id="ludar" class="form-control" >
                    
                    
                    
                    <input type="hidden" id="idA" value="{{ $id }}" name="idA">
                    <input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">
                    
                    <br>
                    <a href="" class="btn btn-info form-control btnEditar"> Editar</a>
                    <br>
                    
                </form>
                
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/denuncias.js') }}"></script>
@endsection