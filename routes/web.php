<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::get('/', function () {
    return view('auth/login');
});
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/home/Finanzas/{id}', 'GastosController@show')->name('detallefinanza');
Route::get('/eliminar/{id}', 'GastosController@destroyGasto')->name('eliminarGastos');
Route::post('/editar/{id}', 'GastosController@update')->name('jajaja');
Route::get('/lista/{id}', 'GastosController@lista')->name('lista');
Route::get('/download/{file}', 'GastosController@archivo')->name('archivo');
// Router actas

Route::get('/s1o1l1i1c1i1t1u1d1e1s', 'ActasController@lista')->name('solicitudes');
Route::get('/i1n1f1o/{id}', 'ActasController@info')->name('informacion');

Route::get('/actasGeneradas', 'actasGeneradasController@lista')->name('aGeneradas');

Route::get('/actasD/{id}', 'ActasController@pdf')->name('pdf');
Route::get('/downloadActa/{file}', 'ActasController@archivo')->name('archivoActa');
Route::post('/EnviarActa', 'ActasController@actualizar')->name('actualizar');
Route::resource('/home/solicitudes', 'ActasController');

Route::resource('/home/actas', 'actasGeneradasController');
Route::resource('/home/Finanzas', 'GastosController');

//ACTIVIDADES victor

Route::get('/home/actividades', 'ActividadesController@index')->name('actividades');
Route::get('/home/actividades/{id}', 'ActividadesController@show')->name('informacionActividades');

Route::get('/actividades/{id}', 'ActividadesController@lista')->name('listaActividades');
Route::get('/eliminarA/{id}', 'ActividadesController@destroyActividad')->name('eliminarActividades');
Route::put('/editarA/{id}', 'ActividadesController@update')->name('editarActividades');

Route::post('/actividades/', 'ActividadesController@store')->name('crearActividad');

//ASOCIACIONES PATO

Route::get('/home/Asociaciones', 'AsociacionesController@index1')->name('asociaciones');
Route::get('/home/Asociaciones/{id}', 'AsociacionesController@show')->name('integrantesasociacion');
Route::get('/listaAsociaciones', 'AsociacionesController@index')->name('listaasociaciones');
Route::get('/Home/Asociaciones/miembrosAsociaciones/{id}', 'AsociacionesController@miembros')->name('miembrosasociaciones');
Route::get('/Home/vercuentas', 'AsociacionesController@cuentas')->name('vercuentas');
Route::get('/Home/vercuentas/{id}', 'AsociacionesController@eliminarCuenta')->name('eliminarCuenta');
Route::put('/editar1/{id}', 'AsociacionesController@update')->name('asociacionesupdate');
Route::post('/Home/Asociaciones', 'AsociacionesController@store')->name('agregarasociacion');
Route::post('/peliminar', 'AsociacionesController@eliminar')->name('eliminarasociacion');
// Denuncias victor

Route::get('/home/denuncias', 'DenunciasController@index')->name('denuncias');
Route::get('/home/denuncias/{id}', 'DenunciasController@show')->name('informacionDenuncias');
Route::get('/denuncias/{id}', 'DenunciasController@lista')->name('listaDenuncias');
Route::post('/Denuncias/', 'DenunciasController@store')->name('crearDenuncia');

Route::get('/eliminard/{id}', 'DenunciasController@destroyDenuncias')->name('eliminarDenuncia');
Route::put('/editard/{id}', 'DenunciasController@update')->name('editarDenuncia');

Route::get('Home/infoFinanciera', 'ActividadesController@verGastos')->name('verGastos');
Route::get('/asociaciones', 'ActividadesController@listarAsociaciones')->name('listarAsociaciones');
Route::get('/AsociacionesF/{id}/{fecha}', 'ActividadesController@AsociacionesF')->name('AsociacionesF');
